# Desenvolver um programa que leia quatro valores e guarde os em uma tupla
# No final mostre:
# Quantas vezes o valor 9 apareceu
# Em posição foi digitado o valor 3
# Quais foram os números pares

print("Digite quatro números")
números = int(input("Digite o 1º número: ")), int(input("Digite o 2º número: ")), \
          int(input("Digite o 3º número: ")), int(input("Digite o ultimo número: "))

print(f"Quantidade de vezes que o número 9: {números.count(9)}" if números.count(9) >= 1 else "Não tem número 9 nesta lista")
print(f"Posição que o número 3 aparece: {números.index(3)+1}" if números.count(3) >= 1 else "Não tem número 3 nesta lista")
print(f"Os números pares foram: ", end=' ')
for i in números:
    if i%2 == 0:
        print(f"{i}", end=' ')
