# O programa deve ler um número 'n' e mostrar na tela os 'n' primeiros elementos de uma sequência de fibonacci

print("Sequência de fibonacci")
termos = int(input("Quantos termos quer mostrar ? "))
n1 = 0
n2 = 1
resultado = 0
ct = 2 #Começa o contador com dois, pois dois termos serão "printados" antes de entrar na função de repetição
print(f"{n1} - {n2}",end=' - ')
while ct < termos:
    resultado = n1 + n2
    print(f"{resultado}",end=' - ')
    n1 = n2
    n2 = resultado
    ct += 1
print("Fim")
