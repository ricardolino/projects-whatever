# Função se chama 'maior', recebe vários parâmetros e mostra o maior e a quantidade de valores passados
# Deve mostrar mensagem diferente caso não seja passado nenhum número

def maior(*num):
    if len(num) == 0:
        print("Não foi passado nunhum parêmetro")
    else:
        for i in range(0, len(num)):
            if i == 0:
                maior_num = num[0]
            elif maior_num < num[i]:
                maior_num = num[i]
        print(f"Maior número: {maior_num}")
        print(f"Quantidade de parâmetros passados: {len(num)}")
    print()


maior()
maior(1, 2, 3)
maior(3, 2, 1)
maior(3, 5, 10, 2)
maior(2, 9, 4, 5, 7, 1)
maior(4, 7, 0)
maior(1, 2)
maior(6)
maior()
