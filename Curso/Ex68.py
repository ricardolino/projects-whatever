# Faça um programa que jogue impar-par com o computador
# O programa para quando o jogador perder, no final deve mostrar o números de vitórias seguidas do jogador

import random, time
ct = 0
while True:
    while True:
        jogada = input("Impar (I) Par (P): ").lower()
        if jogada == 'i' or jogada == 'p':
            break
    n_jogador = int(input("Escolha o seu número: "))
    n_computador = random.randint(0, 10)
    soma = n_jogador + n_computador
    if (soma % 2 == 0 and jogada == 'p') or (soma % 2 != 0 and jogada == 'i'):
        ct += 1
        time.sleep(1)
        print("\nVocê Venceu!")
    else:
        time.sleep(1)
        break

print(f"\nVocê Perdeu! \nVocê conseguiu vencer {ct} vez(es) seguida(s)")
