# Ler o nome e quantidade de partidas de um jogador e guarda num dicionário
# Perguntar e ler a quantidade de gols em cada partida e coloca em uma lista que será guardada no mesmo dicionário
# Guarda também no mesmo dicionário o total de gols
# No final, mostra o conteúdo da lista de forma organizada

cadastro = {'Nome': input("Nome: "), "Quantidade de Partidas": int(input("Quantidade de partidas: "))}
gols_por_partida = []
total_gols = 0

for i in range(1, cadastro['Quantidade de Partidas'] + 1):
    gol = int(input(f'Quantidade de gols na {i}° partida: '))
    gols_por_partida.append(gol)
    total_gols += gol
cadastro['Gols por partida'] = gols_por_partida[:]
cadastro['Total de gols'] = total_gols

print()
for k, v in cadastro.items():
    print(f"{k}: {v}")

print(f"\n{cadastro['Nome']} jogou {cadastro['Quantidade de Partidas']} partidas")
for i, v in enumerate(cadastro['Gols por partida']):
    print(f"   => Quantidade de gols na {i+1}° partida: {v}")
print(f"Total de gols: {cadastro['Total de gols']}")
