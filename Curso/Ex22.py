#Objetivo:
#Usuário digitará o nome e aparecerá o nome maiúsculo, minúsculo, a qtd de letras e a qtd de letras no primeiro nome

nome = str(input('Digite o seu nome completo: ')).strip()
print(f'Nome maiúsculo: {nome.upper()} \nNome minúsculo: {nome.lower()}')
qtd_espaços = nome.count(' ')
print(f'Quantidade de letras: {len(nome)- qtd_espaços}')
local_espaço = nome.find(' ')
print(f'Quantidade de letras no 1º nome: {len(nome[:local_espaço])}')

