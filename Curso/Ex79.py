# Usuário vai digitar varios valores numéricos (quantidade não especificada) e cadastra numa lista
# Caso já tenho o número na lista, este não será adicionado
# No final, mostre os valores que estão na lista em ordem crescente

lista = []

while True:
    continuar = ''
    num = int(input("\nDigite um valor: "))
    if lista.count(num) < 1:
        lista.append(num)
        print(f"Número {num} adicionado com SUCESSO")
    else:
        print(f"ERRO, número {num} já existe na lista")
    while 'S' not in continuar and 'N' not in continuar:
        continuar = input("Deseja continuar [S/N]: ").upper()
    if continuar == 'N':
        break

lista.sort()
print(f"\nLista em ordem crescente: {lista}")
