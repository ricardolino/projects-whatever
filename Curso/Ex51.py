# O programa ler o primeiro termo e a razão da progressão aritmética e mostrar os 10 primeiros termos

termo = int(input('Primeiro número: '))
razão = int(input('Razão: '))

for i in range(termo, (razão*10 + termo), razão):
    print(f"{i} - ", end="")
print("Fim")
