# O programa deve ler dois valores e montrar um menu
# 1 - Somar, 2 - Multiplicar, 3 - Maior, 4 - Novos Números (caso o usuário queira entrar com novos números), 5 - Sair do programa

n1 = int(input("Digite o primeiro número: "))
n2 = int(input("Digite o segundo número: "))
sair = False
while not sair:
    operação = int(input("\n1 - Soma \n2 - Multiplicação \n3 - Maior"
                         "\n4 - Novos números \n5 - Sair do programa"
                         "\nQual operação ? "))
    if operação == 1:
        print(f"Soma = {n1 + n2}")
    elif operação == 2:
        print(f"Multiplicação = {n1 * n2}")
    elif operação == 3:
        if n1 > n2:
            print(f"Maior número = {n1}")
        elif n2 > n1:
            print(f"Maior número = {n2}")
        else:
            print("Números Iguais")
    elif operação == 4:
        print("Escolha novos números")
        n1 = int(input("Digite o primeiro número: "))
        n2 = int(input("Digite o segundo número: "))
    elif operação == 5:
        sair = True
    else:
        print("Caracter Inválido")

print("\nFim do programa!")

