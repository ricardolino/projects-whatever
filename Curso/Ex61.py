# Refazer o exercício 51 usando while

termo = int(input("Primeiro Termo: "))
razão = int(input("Razão: "))
ct = 0
while ct < 10:
    print(f"{termo} - ", end="")
    termo += razão
    ct += 1
print("Fim")
