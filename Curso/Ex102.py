# Criar função chamada fatorial()
# Ela recebe dois parâmetros número (q vai ser calculado) e show (q recebe um valor lógico (True or False))
# O parâmetro show é opcional indicando se vai mostra ou não o calculo

def fatorial(n, show=False):
    """
    --> Calcula fatorial de um número
    :param n: O número a ser calculado
    :param show: (Opcional) Mostra ou não a conta
    :return: O valor fatorial de um número
    """
    result = 1
    print(f"{n}! =",end=' ')
    for i in range(n, 0, -1):
        result *= i
        if show:
            print(f"{i}",end=' ')
            print("x" if i > 1 else "=", end=' ')
    return result

if __name__ == '__main__':
    print(fatorial(3))
    print()
    help(fatorial)
    print(fatorial(3, show=True))
