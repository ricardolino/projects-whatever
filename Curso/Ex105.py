# Criar função chamada notas()
# Ele vai receber varias notas e retorna um dicionário com:
# Quantidade de notas, maior nota, menor nota, média da turma, e situação (opcional)
# A função deve ter docstring

def notas(*n, sit=False):
    """
    Recebe os paramêtros e adiciona a um dicionário
    :param n: Parâmetro obridatório, recebe os valores (aceita quantidade ilimitada de valores)
    :param sit: Parâmetro opcional, recebe um valor lógico
    :return: Retorna os valores organizados em um dicionário
    """
    dic = dict()
    dic["Quantidade de notas"] = len(n)
    dic["Maior nota"] = max(n)
    dic["Menor nota"] = min(n)
    dic["Média"] = sum(n)/len(n)
    if sit:
        if dic["Média"] >= 7:
            dic["Situação"] = "Bom"
        elif dic["Média"] >= 5:
            dic["Situação"] = "Ok"
        else:
            dic["Situação"] = "Ruim"
    return dic

if __name__ == '__main__':
    resp = notas(5.5, 9.5, 10, 6.5, sit=True)
    print(resp)
    print()
    help(notas)
