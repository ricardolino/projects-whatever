#Objetivo:
#Mostrar unidade, dezena, centena, milhar de um número entre 0 e 9999.

n = str(input('Escreva um número entre 0 e 9999: ')).strip()

if len(n) == 1:
    print(f'Unidade: {n} \nDezena: 0 \nCentena: 0 \nMilhar: 0')
elif len(n) == 2:
    print(f'Unidade: {n[1]} \nDezena: {n[0]} \nCentena: 0 \nMilhar: 0')
elif len(n) == 3:
    print(f'Unidade: {n[2]} \nDezena: {n[1]} \nCentena: {n[0]} \nMilhar: 0')
elif len(n) == 4:
    print(f'Unidade: {n[3]} \nDezena: {n[2]} \nCentena: {n[1]} \nMilhar: {n[0]}')
else:
    print('Deu bosta')


#Outra forma de se resolver (forma matemática)
#n = int(input('Digite um número entre 0 e 9999: '))
#print(f'Unidade: {n // 1 % 10} \nDezena: {n // 10 % 10} '
#      f'\nCentena: {n // 100 % 10} \nMilhar: {n // 1000 % 10}')