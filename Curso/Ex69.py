# Faça um programa leia a idade sexo das pessoas
# A cada pessoa cadastrada o programa deve pergutar se o usuário quer continuar
# No final deve mostrar a quantidade de:
# 1 - pessoas com mais de 18
# 2 - homens que foram cadastrados
# 3 - mulheres com menos de 20 anos
# O programa deve repetir a pergunta caso o usuário digite o caracter errado

ct_mais18 = 0
ct_homens = 0
ct_mulheres20 = 0
continuar = 's'
while continuar == 's':
    print("-"*20)
    idade = int(input("Idade: "))
    sexo = ''
    while sexo != 'm' and sexo != 'f':
        sexo = input("Sexo [M/F]: ").lower()
    if idade >= 18:
        ct_mais18 += 1
    if sexo == 'm':
        ct_homens += 1
    if sexo == 'f' and idade < 20:
        ct_mulheres20 += 1
    continuar = ''
    while continuar != 's' and continuar != 'n':
        continuar = input("Quer continuar [S/N]? ").lower()

print("*"*45)
print(f"Quantidade de pessoas com mais de 18 anos: {ct_mais18}"
      f"\nQuantidade de homens cadastrados: {ct_homens}"
      f"\nQuantidade de mulheres com menos de 20 anos: {ct_mulheres20}")
