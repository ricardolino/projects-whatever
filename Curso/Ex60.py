# Faça um programa que leia um número e mostre seu fatorial
# Obs: Fazer com 'for' e 'while', mostrar o ponto de exclamação no número fatorado

import math

n_fatorado = int(input("Escolha o número que vai ser fatorado: "))

print("\nFatorar usando o módulo math")
resultado3 = math.factorial(n_fatorado)
print(f"{n_fatorado}! =", end=" ")
for i in range(n_fatorado, 0, -1):
    print(f"{i}", end="")
    print(" x " if i > 1 else " = ", end="")
print(f"{resultado3}")

print("\nFatorando usando o while: ")
print(f"{n_fatorado}! = {n_fatorado}", end=" ")
resultado2 = 1
n_fatorado_especifico_para_o_while = n_fatorado - 1
while(n_fatorado_especifico_para_o_while > 0):
    print(f"x {n_fatorado_especifico_para_o_while}", end=" ")
    resultado2 = resultado2 * n_fatorado
    n_fatorado -= 1
    n_fatorado_especifico_para_o_while -= 1
print(f"= {resultado2}")