import moeda

num = float(input("Digite um valor: "))
print(f"Metade: {moeda.metade(num)}"
      f"\nDobro: {moeda.dobro(num)}"
      f"\nAumentando 10%: {moeda.aumentar(num, 10)}"
      f"\nDiminuindo 13%: {moeda.diminuir(num, 13)}")