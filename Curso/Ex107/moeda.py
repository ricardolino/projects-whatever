def metade(valor):
    return valor / 2


def dobro(valor):
    return valor * 2


def aumentar(valor, p):
    return valor + (valor * (p / 100))


def diminuir(valor, p):
    return valor - (valor * (p / 100))
