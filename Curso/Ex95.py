# Aprimorar o Ex93
# Adiconando quantidade ilimitada de jogadores que podem ser cadastrados
# Mostrando uma tabela com todos os jogadores, médias e total de gols
# Adicionar tambem a funcionalidade de ver o desempenho específico de um jogador em cada partida

jogadores = []
cadastro = {}
while True:
    cadastro.clear()
    cadastro = {'Nome': input("Nome: "), "Quantidade de Partidas": int(input("Quantidade de partidas: "))}
    gols_por_partida = []
    total_gols = 0
    for i in range(1, cadastro['Quantidade de Partidas'] + 1):
        gol = int(input(f'Quantidade de gols na {i}° partida: '))
        gols_por_partida.append(gol)
        total_gols += gol
    cadastro['Gols por partida'] = gols_por_partida[:]
    cadastro['Total de gols'] = total_gols
    jogadores.append(cadastro.copy())
    continuar = ''
    while ('S' not in continuar) and ('N' not in continuar):
        continuar = input("Deseja continuar ? [S/N]: ").strip().upper()
    print()
    if continuar == 'N':
        break

print(f"{'ID':<4}{'NOME':<15}{'MÉDIA':<8}{'TOTAL DE GOLS':>10}")
for id, v in enumerate(jogadores):
    media = v['Total de gols'] / v['Quantidade de Partidas']
    print(f"{id:<4}{v['Nome']:<15}{media:<8.1f}{v['Total de gols']:>10}")

while True:
    print()
    analise = int(input("Digite [999] caso queira sair \nVer dados [Id]: "))
    if analise == 999:
        break
    elif (analise < 0) or (analise > len(jogadores) - 1):
        print('Não existe este Id na tabela')
    else:
        print(f"\n{jogadores[analise]['Nome']} jogou {jogadores[analise]['Quantidade de Partidas']} partidas")
        for i, v in enumerate(jogadores[analise]['Gols por partida']):
            print(f"   => Quantidade de gols na {i+1}° partida: {v}")
        print(f"Total de gols: {jogadores[analise]['Total de gols']}")
