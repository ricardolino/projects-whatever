import moeda

num = float(input("Digite um valor: R$"))
print(f"Metade: {moeda.moeda(moeda.metade(num))}"
      f"\nDobro: {moeda.moeda(moeda.dobro(num))}"
      f"\nAumentando 10%: {moeda.moeda(moeda.aumentar(num, 10))}"
      f"\nDiminuindo 13%: {moeda.moeda(moeda.diminuir(num, 13))}")
