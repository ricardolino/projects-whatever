def metade(valor=0):
    return valor / 2


def dobro(valor=0):
    return valor * 2


def aumentar(valor=0, p=0):
    return valor + (valor * (p / 100))


def diminuir(valor=0, p=0):
    return valor - (valor * (p / 100))


def moeda(valor=0, moeda='R$'):
    return f"{moeda}{valor:.2f}".replace('.', ',')
