def leiadinheiro(msg):
    while True:
        valor = input(msg).replace(',','.')
        if verificar(valor):
            return float(valor)
        else:
            print("ERRO! Digite um valor monetário")

def verificar(valor):
    try:
        float(valor)
    except ValueError:
        return False
    return True
