# O usuário irá digitar uma expressão qualquer que use parênteses
# O programa deve analisar se os parênteses abertos ou fechados, retornando se a expressão é válida ou não

# expressao = input("Digite uma expressão:\n")
# abertos = fechados = 0
# if expressao.count('(') == expressao.count(')'):
#     for pos, caracter in enumerate(expressao):
#         if caracter == '(':
#             abertos +=1
#         elif caracter == ')':
#             fechados += 1
#         if fechados > abertos:
#             print("Expressão inválida")
#             break
#         elif pos == len(expressao)-1:
#             print("Expressão válida")
# else:
#     print("Expressão inválida")

expressao = input("Digite uma expressão:\n")
analisando_expressão = []
if expressao.count('(') == expressao.count(')'):
    for caracter in expressao:
        if caracter == '(':
            analisando_expressão.append('(')
        elif caracter == ')':
            if len(analisando_expressão) > 0:
                analisando_expressão.pop()
            else:
                analisando_expressão.append(')')
                break
    print("Expressão válida" if ')' not in analisando_expressão else "Expressão inválida, você fechou um parêntese antes de abrir")
else:
    print("Expressão inválida, número de parênteses abertos e fechados diferentes")
