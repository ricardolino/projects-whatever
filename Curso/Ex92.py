# Ler o nome, ano de nascimento, id da carteira de trabalho (digitar 0 se não tiver)
# Caso tenha carteira de trabalho ler o ano da contratação e o salário
# No final mostrar os dados dentro do dicionário de forma organizada

import datetime
Carteira_de_Identificação = {}

Carteira_de_Identificação['Nome'] = input("Nome: ")
Carteira_de_Identificação['Idade'] = datetime.date.today().year - int(input("Ano de Nascimento: "))
Carteira_de_Identificação['Carteira de trabalho (Id)'] = int(input("Id da Carteira de Trabalho: "))

if Carteira_de_Identificação['Carteira de trabalho (Id)'] != 0:
    Carteira_de_Identificação['Ano de Aposentadoria'] = datetime.date.today().year + (35 - (datetime.date.today().year - int(input("Ano de Contratação: "))))
    Carteira_de_Identificação['Salário R$'] = input("Salário: R$")

print()
for k, v in Carteira_de_Identificação.items():
    print(f"{k}: {v}")
