# Cria função chamada voto(), ela recebe o ano de nascimento
# Mostra ou retorna indicando se a pessoa tem voto NEGADO, OBRIGATÓRIO OU OPCIONAL.

def voto():
    import datetime
    idade = datetime.date.today().year - ano_nascimento
    if 18 <= idade < 65:
        print("Voto Obrigatório")
    elif 16 <= idade < 18 or idade >= 65:
        print("Voto Opcional")
    else:
        print("Voto Negado")

if __name__ == '__main__':
    ano_nascimento = int(input("Ano de Nascimento: "))
    voto()
