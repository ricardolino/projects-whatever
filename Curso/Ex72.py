# Crie um programa que tenha uma tupla preenchida com uma contagem por extenso de zero a vinte
# Seu programa deverá ler um número pelo tecaldo (entre 0 e 20) e mostrá-lo por extenso

numeros_extenso = 'zero', 'um', 'dois', 'três', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove', 'dez', 'onze', 'doze', 'treze', 'quatorze', 'quinze', 'dezesseis', 'dezesete', 'dezoito', 'dezenove', 'vinte'

while True:
    numero_escolhido = -1
    while numero_escolhido < 0 or numero_escolhido > 20:
        numero_escolhido = int(input("Escolha um número entre 0 e 20: "))
    print(f"O número {numero_escolhido} por extenso é {numeros_extenso[numero_escolhido]}")
    while True:
        continuar = input("Deseja com continuar [S/N]: ").lower().strip()
        if continuar == 's' or continuar == 'n':
            break
    if continuar == 'n':
        break
