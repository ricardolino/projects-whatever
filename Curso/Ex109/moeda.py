def metade(valor=0, m=False):
    res = valor / 2
    if m:
        return moeda(res)
    else:
        return res


def dobro(valor=0, m=False):
    res = valor * 2
    if m:
        return moeda(res)
    else:
        return res


def aumentar(valor=0, p=0, m=False):
    res = valor + (valor * (p / 100))
    if m:
        return moeda(res)
    else:
        return res


def diminuir(valor=0, p=0, m=False):
    res = valor - (valor * (p / 100))
    if m:
        return moeda(res)
    else:
        return res


def moeda(valor=0, moeda='R$'):
    return f"{moeda}{valor:.2f}".replace('.', ',')
