import moeda

num = float(input("Digite um valor: R$"))
print(f"Metade: {moeda.metade(num, True)}"
      f"\nDobro: {moeda.dobro(num, True)}"
      f"\nAumentando 10%: {moeda.aumentar(num, 10, True)}"
      f"\nDiminuindo 13%: {moeda.diminuir(num, 13, True)}")
