# Criar um program que deve ler o o nome, idade e sexo de 4 pessoas
# Deve mostrar a média da idade, o nome do homem mais velha e a quantidade de mulhres com menos de 20 anos

# Estou pesando seriamente em usar class
# Ou ver  video do curso e fazer depois, estou com dúvida e preguiça de fazer o resto

#Usando class
# class identificação(object):
#     def __init__(self, nome, idade, sexo):
#         self.n = nome
#         self.i = idade
#         self.s = sexo
#
# if __name__ == '__main__':
#     pessoa = [0] * 4
#
#     for i in range(0, len(pessoa)):
#         pessoa[i] = identificação(input("\nNome: ").strip(), int(input("Idade: ")),
#                                   input("M para masculino e F para feminino \nSexo: ").strip().lower())
#
#     soma = 0
#     idade_pessoa_velha = pessoa[0].i
#     pessoa_velha = pessoa[0].n
#     qtd_mulheres_jovens = 0
#     for n in range(0, len(pessoa)):
#         soma += pessoa[n].i
#         if pessoa[n].i > idade_pessoa_velha: # E se houver duas pessoas com a maior idade do grupo
#             pessoa_velha = pessoa[n].n# E se houver duas pessoas com a maior idade do grupo
#         if pessoa[n].s == 'f':
#             if pessoa[n].i < 20:
#                 qtd_mulheres_jovens += 1
#     media = soma/len(pessoa)
#
# print(f"\n\nMédia de idade: {media:.2f} \nPessoa mais velha: {pessoa_velha}"
#       f"\nQuantidade de mulheres com menos de 20 anos: {qtd_mulheres_jovens}")# E se houver duas pessoas com a maior idade do grupo

# Jeito mais simples e correto
soma = 0
idade_maisvelha = 0
ct_mulheresnovas = 0
nome_pessoamaisvelha = ''

for i in range(0, 4):
    print(f"--------{i+1}° Pessoa --------")
    nome = input("Nome: ").strip()
    idade = int(input("Idade: "))
    sexo = input("Sexo [M/F]: ").strip().upper()
    soma += idade
    if sexo == 'M' and idade > idade_maisvelha:
        idade_maisvelha = idade
        nome_pessoamaisvelha = nome
    if sexo == 'F' and idade < 20:
        ct_mulheresnovas +=1
media = soma/4

print(f"Média de idade: {media:.1f} \nNome do homem mais velho: {nome_pessoamaisvelha}"
      f"\nQuantidade de mulheres com menos de 20 anos: {ct_mulheresnovas}")
