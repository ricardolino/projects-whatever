# Ler o nome, sexo e idade de várias pessoas guardando em dicionário
# Em seguida guarda cada dicionário em uma lista
# No final mostre:
# Quantas pessoas foram cadastradas
# A média de idade do grupo
# Uma lista com todas as mulheres
# Uma lista com todas as pessoas com idade acima da média

tabela = list()

while True:
    cadastro = {'Nome': input("Nome: ").strip().title()}
    sexo = ''
    while ('M' not in sexo) and ('F' not in sexo):
        sexo = input("Sexo [M/F]: ").strip().upper()
    cadastro['Sexo'] = sexo
    cadastro['Idade'] = int(input("Idade: "))
    tabela.append(cadastro.copy())
    continuar = ''
    while ('S' not in continuar) and ('N' not in continuar):
        continuar = input("Deseja continuar ? [S/N]: ").strip().upper()
    print()
    if continuar == 'N':
        break

idades_somadas = 0
mulheres = []
maior_media = []
for i in tabela:
    idades_somadas += i['Idade']
    if i['Sexo'] == 'F':
        mulheres.append(i['Nome'])
media = idades_somadas/len(tabela)

print(f"\nQuantidade de pessoas cadastradas: {len(tabela)}"
      f"\nMédia de idade das pessoas cadastradas {media}")
print(f"\nLista de mulheres cadastradas:")
for i in mulheres:
    print(i, end=' ')
print(f"\nLista com as pessoas acima da média de idade ({media}): ")
for i in tabela:
    if i['Idade'] > media:
        print(i)
