#Objetivo:
#Calcula hipotenusa a aparte dos catetos disponibilizados pelo usuário

ct_oposto = float(input('Cateto oposto: '))
ct_adjacente = float(input('Cateto adjacente: '))

print(f'\nCateto oposto = {ct_oposto}\nCateto adjacente = {ct_adjacente}\n'
      f'Hipotenusa = {((ct_oposto**2)+(ct_adjacente**2))**(1/2):.2f}')
