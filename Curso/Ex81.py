# Ler varios valores, coloca-los numa lista
# No final mostre:
# Quantos números foram digitados
# Os valores em ordem decrecente
# Se o valor 5 está na lista

lista = []
continuar = 'S'

while continuar == 'S':
    num = int(input("\nDigite um número: "))
    lista.append(num)
    continuar = ''
    while continuar != 'S' and continuar != 'N':
        continuar = input("Deseja continuar [S/N]: ").strip().upper()

print(f"\n{lista} \nQuantidade números: {len(lista)}")
lista.sort(reverse=True)
print(f"Ordem decrecente: {lista}")
print("O valor 5 está na lista" if 5 in lista else f"O valor 5 não está na lista")
