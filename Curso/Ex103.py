# Criar função chamada ficha
# Recebe dois parâmetros opcionais:nome do jogador e gols
# O programa deverá ser capaz de mostra a ficha do jogador mesmo se um dos dados não for informado

def ficha(n='<desconhecido>', g='0'):
    print(f"O jogador {n} fez {g} gols")


if __name__ == '__main__':
    nome = input("Nome: ").strip()
    gols = input("Quantidade de gols: ")
    if nome == '' and gols.isnumeric():
        ficha(g=gols)
    elif nome != '' and gols.isnumeric() == False:
        ficha(nome)
    elif nome == '' and gols.isupper() == False:
        ficha()
    else:
        ficha(nome, gols)
