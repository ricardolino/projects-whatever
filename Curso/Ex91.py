# 4 jogadores vão joga um dado
# Os resultados tem que ser guardados em um dicionário
# No final coloque o dicionário em ordem

import operator, random, time

jogadores = {'Jogador1': random.randint(1, 6), 'Jogador2': random.randint(1, 6),
             'Jogador3': random.randint(1, 6), 'Joagado4': random.randint(1, 6)}
ranking = dict()
for k, v in jogadores.items():
    print(f"{k}: {v}")
    time.sleep(1)

ranking = sorted(jogadores.items(), key=operator.itemgetter(1), reverse=True)
print()
for i, v in enumerate(ranking):
    print(f"{i+1}ª - {v[0]} com {v[1]}pts ")
    time.sleep(1)
