from lib3.interface2.main import *

def arquivoExiste(nome):
    try:
        a = open(nome, 'rt')
        a.close()
    except FileNotFoundError:
        return False
    else:
        return True


def criarArquivo(nome):
    try:
        a = open(nome, 'wt+')
        a.close()
    except:
        print("Houve um ERRO na criação do arquivo")
    else:
        print(f"Arquivo {nome} criado com SUCESSO")


def lerArquivo(nome):
    cabeçalho("Pessoas Cadastradas")
    try:
        a = open(nome, 'rt')
    except:
        print("ERRO ao ler o aquivo")
    else:
        print(a.read())

