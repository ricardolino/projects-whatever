from lib2.interface import *
from lib2.arquivo import *
import time

arq = "curso.txt"

if not arquivoExiste(arq):
    criarArquivo(arq)

while True:
    res = menu(["Ver lista", "Cadastrar", "Sair do programa"])
    if res == 1:
        #Ler o arquivo
        lerArquivo(arq)
    elif res == 2:
        cabeçalho("Cadastrar Pessoas")
        nome = input("Nome: ").strip()
        idade = leiaInt("Idade: ")
        cadastrar(arq, nome, idade)
    elif res == 3:
        cabeçalho("Finalizando o programa")
        break
    else:
        print("ERRO! Digite um número válido")
    time.sleep(1)
