from lib2.interface import *

def arquivoExiste(nome):
    try:
        a = open(nome, 'rt')
        a.close()
    except FileNotFoundError:
        return False
    else:
        return True


def criarArquivo(nome):
    try:
        a = open(nome, 'wt+')
        a.close()
    except:
        print("Houve um ERRO na criação do arquivo")
    else:
        print(f"Arquivo {nome} criado com SUCESSO")


def lerArquivo(nome):
    try:
        a = open(nome, 'rt')
    except:
        print("ERRO ao ler o aquivo")
    else:
        cabeçalho("Pessoas Cadastradas")
        for l in a:
            dado = l.split(';')
            dado[1] = dado[1].replace('\n','')
            print(f"{dado[0]:<25}{dado[1]:>5} anos")
    finally:
        a.close()


def cadastrar(n_A, n_P='<desconhecido>', i=0):
    try:
        a = open(n_A, 'at')
    except:
        print(f"Houve um ERRO na criação do arquivo {n_A}")
    else:
        try:
            a.write(f"{n_P};{i}\n")
        except:
            print(f"Houve um ERRO no registro do {n_P}")
        else:
            print(f"{n_P} cadastrado com sucesso")
            a.close()
