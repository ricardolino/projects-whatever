def leiaInt(msg):
    while True:
        try:
            num = input(msg).strip()
            num = int(num)
        except (ValueError, TypeError):
            print("ERRO! Digite um número Inteiro")
            continue
        except (KeyboardInterrupt):
            print()
            return 0
        else:
            return int(num)


def linha():
    return "-"*35


def cabeçalho(txt):
    print(linha())
    print(f"{txt.center(35)}")
    print(linha())


def menu(lista):
    cabeçalho("MENU PRINCIPAL")
    for n, i in enumerate(lista):
        n+=1
        print(f"{n} - {i}")
    print(linha())
    op = leiaInt("Sua Opção: ")
    return op