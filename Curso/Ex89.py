# Leia o nome e duas notas de vários alunos e guarde em uma lista
# No final mostre, um boletim com as médias de cada um
# Permita que o usuário mostre as duas notas de cada aluno individualmente

aluno = []
boletim = []

while True:
    aluno.append(input("\nNome: "))
    aluno.append(float(input("Nota 1: ")))
    aluno.append(float(input("Nota 2: ")))
    boletim.append(aluno[:])
    aluno.clear()
    continuar = ''
    while ('S' not in continuar) and ('N' not in continuar):
        continuar = input("Deseja continuar ? [S/N]: ").upper().strip()
    if 'N' in continuar:
        break

print()
print(f"{'Id':<4}{'Nome':<9}{'Média':>8}")
for i in range(0, len(boletim)):
    media = (boletim[i][1] + boletim[i][2])/2
    print(f"{i:<4}{boletim[i][0]:<9}{media:>8.1f}")

while True:
    ver_nota = int(input("\nDeseja ver as notas de qual aluno? (999 interrompe) \nId:  "))
    if ver_nota == 999:
        break
    print(boletim[ver_nota])

print("\nFIM DO PROGRAMA")
