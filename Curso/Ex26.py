#Objetivo:
#Mostrar a qtd, a primeira e a ultimo aparição da letra a.

frase = input('Digite qualquer frase: ').strip().lower()

print('Analisando a a frase "{}"... \nA letra A aparece {} vezes'
      .format(frase, frase.count('a')))
print(f"A letra a aparece pela primeira vez na posição: {frase.find('a') +1}")
print(f"A letra a aparece pela ultima vez na posição: {frase.rfind('a') + 1}")
