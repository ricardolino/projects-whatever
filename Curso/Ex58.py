# Melhorar o jogo do Ex28
# Aonde o computador vai pensar num número entre 0 a 10 e o jogador vai tentar advinhar até acertar
# Mostrando o número de tentativas foram necessárias para vencer

import random

n_sorteado = random.randint(0, 10)
ct_tentativas = 0
n_jogador = -1
while(n_jogador != n_sorteado):
    n_jogador = int(input("Escolha um número de 0 a 10: "))
    ct_tentativas += 1
    if n_jogador < n_sorteado:
        print(f"MAIS do que {n_jogador}...")
    elif n_jogador > n_sorteado:
        print(f"MENOS do que {n_jogador}...")

print(f"\nACERTOU! \nVocê precisou de {ct_tentativas} tentativas para acerta")
