#O programa deve dizer qual o maior valor, entre os dois números digitados

primeiro_valor = int(input('Digite o primeiro valor: '))
segundo_valor = int(input('Digite o segundo valor: '))

if primeiro_valor > segundo_valor:
    print('O primeiro valor é maior')
elif segundo_valor > primeiro_valor:
    print('O segundo valor é maior')
else:
    print('Os valores são iguais')

