#O usuário deve advinhar o número que o computador está "pensando". Deve aparecer vencer e perdeu.

import random, time

numero_escolhido = int(input('Escolha um número de 0 e 5: '))

numero_sorteado = random.randint(0, 5)

time.sleep(1)
print('Processando...')
time.sleep(1)
print('Você ganhou' if numero_sorteado == numero_escolhido else 'Você perdeu')
