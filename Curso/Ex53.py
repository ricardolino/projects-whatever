# Descobrir se a frase é um polídromo

frase = input('Digite uma frase: ').strip().lower().split()

print(f"\nO inverso de {''.join(frase)} é {''.join(frase)[::-1]}\n")

if ''.join(frase) == ''.join(frase)[::-1]:
    print('Esta frase é um PALÍDROMO!')
else:
    print('Esta frase NÃO é um PALÍDROMO!')

