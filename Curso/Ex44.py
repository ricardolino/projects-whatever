# Calcule o valor a ser pago seguindo as condições de pagamento propostas

preço = float(input('Valor do produto: R$'))
forma_pagamento = int(input('1 - Á vista dinheiro/cheque (10% de desconto)'
                            '\n2 - Á vista cartão (5% de desconto)'
                            '\n3 - Em até 2x no cartão (permanece o mesmo preço)'
                            '\n4 - 3x ou mais no cartão (20% de juros)'
                            '\nQual a será a forma de pagamento ? (Use os números): '))
print('\n')#Só para estética
total = preço
if forma_pagamento == 1:
    total = preço - (preço * 0.1)
elif forma_pagamento == 2:
    total = preço - (preço * 0.05)
elif forma_pagamento == 3:
    total = preço
    print(f'As 2 parcelas ficaram em R${total / 2:.2f}')
elif forma_pagamento == 4:
    total = preço + (preço * 0.2)
    parcelas = int(input('Quantidade de parcelas: '))
    print(f'As {parcelas} parcelas ficaram em R${total / parcelas:.2f}')
else:
    print('Digito Inválido!')

print(f'Preço Final do produto: R${total:.2f}')
