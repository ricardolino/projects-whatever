# Criar uma matriz 3x3, preenchida por valores digitados pelo usuário
# No final mostre a matriz na tela com a formatação correta

linha = []
matriz = []

for i in range(3):
    for j in range(3):
        linha.append(int(input("Digite um número: ")))
    matriz.append(linha[:])
    linha.clear()

print('\n')
for i in range(3):
    for j in range(3):
        print(f"[{matriz[i][j]:^5}]",end='')
    print()
