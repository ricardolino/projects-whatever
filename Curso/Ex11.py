#Objetivo:
#Calcula a area e determina a qtd de tinta para pintar uma parede.
#Sabendo que cada pote de tinta pinta dois metros quadrados.

altura = int(input('Altura: '))
largura = int(input('Largura: '))

print(f'Área: {altura*largura}')
print(f'Quantidade de tinta necessária: {(altura*largura)/2}')
