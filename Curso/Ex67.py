# Faça um programa que mostra a tabuada de vários números um de cada vez
# O programa deve parar quando o número digitado for negativo

n = 0
print("Tabuada")
while n >= 0:
    n = int(input("\nDigite um número: "))
    if n < 0:
        break
    for i in range(1, 11):
        print(f"{i} x {n} = {i*n}")
print("\nFim do programa")
