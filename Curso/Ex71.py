# Faça um programa que simule um caixa eletrônico
# O programa ler o valor a ser sacado e informa quantas cédulas de cada valor serão entregues
# Considere as cédulas de 50, 20, 10 e 1

saque = int(input("Saque: R$"))
cedula = 50
ct_cedulas = 0

while True:
    if saque >= cedula:
        saque -= cedula
        ct_cedulas += 1
    else:
        print(f'Quantidade de cédulas de R${cedula}: {ct_cedulas}')
        if cedula == 50:
            cedula = 20
        elif cedula == 20:
            cedula = 10
        else:
            cedula = 1
        ct_cedulas = 0
        if saque == 0:
            break
