# Aprimorar o exercício anterior, mostrando no final:
# A soma de todos os valores pares
# A soma dos valores da terceira coluna
# O maior valor da segunda linha

linha = []
matriz = []
soma_par = soma_coluna3 = 0

for i in range(3):
    for j in range(3):
        num = int(input("Digite um número: "))
        linha.append(num)
        if num%2 == 0:
            soma_par += num
        if j == 2:
            soma_coluna3 += num
    matriz.append(linha[:])
    linha.clear()

print('\n')
for i in range(3):
    for j in range(3):
        print(f"[ {matriz[i][j]:^5} ]",end='')
    print('')

print(f"Soma dos valores pares: {soma_par} "
      f"\nSoma dos valores da terceira coluna: {soma_coluna3}"
      f"\nMaior valor da segunda linha: {max(matriz[1])}")
