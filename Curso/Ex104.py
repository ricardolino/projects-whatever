# Criar função chamda leiaInt()
# Ela funciona de forma semelhante ao input(), só que ela faz a validação para aceitar apenas um valor númerico

def leiaInt(msg):
    while True:
        n = str(input(msg))
        if n.isdecimal():
            valor = int(n)
            break
        print("ERRO! Digite um número inteiro")
    return valor

if __name__ == '__main__':
    n = leiaInt("Digite um número inteiro: ")
    print(f"Você digitou o número {n}")
