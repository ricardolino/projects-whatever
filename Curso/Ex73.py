# Crie uma tupla com a tabela do Brasileirão seria A 2019
# O programa deve mostrar:
# Os cinco primeiros colocados
# Os ultimos 4 colocados
# Uma lista com os times em ordem alfabética
# Em que posição está o time da Chapeconense

tabela = 'Flamengo', 'Santos', 'Palmeiras', 'Grêmio', 'Athletico-PR', 'São Paulo', 'Internacional', 'Corinthians', 'Fortaleza', 'Goiais', 'Bahia', 'Vasco da Gama', 'Atlético-MG', 'Fluminense', 'Botafogo', 'Ceará SC', 'Cruzeiro', 'Cruzeiro', 'CSA', 'Chapecoense', 'Avaí'

print(f"G5: {tabela[:5]} \nZ4: {tabela[-4:]} \nOrdem ALfabética: {sorted(tabela)} \nPosição da Chapecoense: {tabela.index('Chapecoense')}º")
