# Criar uma tupla com nomes de produtos e seus respectivos preços
# No final, mostre uma listagem de preços de forma tabular

material_preço = 'Caderno', 30.00, 'Lápis', 5.00, 'Mochila', 60.00, 'Estojjo', 15.00, 'Livro', 50.00

print("Tabela de Preços")
for i in range(0, len(material_preço), 2):
    print(f"{material_preço[i]:.<30}", end='')
    print(f"R$ {material_preço[i+1]:>5.2f}")
