# Faça um programa que leia o nome e o preço de vários produtos
# O programa deve perguntar se o usuário vai continuar
# No final ele deve mostrar:
# 1 - total de gastos
# 2 - quantos produtos custom mais de R$1000
# 3 - qual o nome do produto mais barato
# O programa deve repetir a pergunta caso o usuário digite o caracter errado

total_compra = 0
produtos_mais1000 = 0
produto_mais_barato = ''
produto_mais_barato_preço = 0
continuar = 's'
while continuar == 's':
    print('-'*25)
    nome_produto = input("Nome do Produto: ")
    preço_produto = float(input("Preço do produto: R$"))
    total_compra += preço_produto
    if preço_produto > 1000:
        produtos_mais1000 += 1
    if produto_mais_barato_preço == 0 or produto_mais_barato_preço > preço_produto:
        produto_mais_barato_preço = preço_produto
        produto_mais_barato = nome_produto
    continuar = ''
    while continuar != 's' and continuar != 'n':
        continuar = input("Quer continuar? [S/N]: ").lower()
print("*"*50)
print(f"Total da compra: R${total_compra:.2f}"
      f"\nQuantidade de produtos custando mais de R$1000: {produtos_mais1000}"
      f"\nProduto mais barato: {produto_mais_barato}   Preço: R${produto_mais_barato_preço:.2f}")
