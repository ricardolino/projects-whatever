#Programa deve ler o ano de nascimento e mostrar a categoria do mesmo

import datetime

ano_nascimento = int(input('Digite o ano nascimento: '))

idade = datetime.date.today().year - ano_nascimento

if idade <= 9:
    print('Categoria: Mirim')
elif idade <= 14:
    print('Categoria: Infantil')
elif idade <= 19:
    print('Categoria: Junior')
elif idade <= 25:
    print('Categoria: Sênior')
else:
    print('Categoria: Master')

