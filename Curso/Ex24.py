#Objetivo:
#Mostrar se o nome da cidade inserido começa e tem Santo.

cidade = input('Digite o nome da cidade: ').strip().title()

if cidade[:5] == 'Santo':
    print('O nome da cidade começa com Santo')
elif cidade[:5] != 'Santo' and 'Santo' in cidade:
    print('O nome da cidade não começa com Santo, mas tem Santo no nome')
else:
    print('A cidade não tem Santo no nome')
