# Ler o nome e peso de várias pessoas. Mostre no final:
# Quantas pessoas foram cadastradas
# As pessoas mais pesadas
# As pessoas mais leves

pessoas = []
pessoas_cadastradas = []
pesado = 0
leve = 0

while True:
    pessoas.append(input("\nNome: "))
    pessoas.append(float(input("Peso: ")))
    pessoas_cadastradas.append(pessoas[:])
    pessoas.clear()
    continuar = ''
    while ('S' not in continuar) and ('N' not in continuar):
        continuar = input("Deseja continuar: ").strip().upper()
    if 'N' in continuar:
        break

for analise in range(len(pessoas_cadastradas)):
    if analise == 0:
        pesado = pessoas_cadastradas[analise][1]
        leve = pessoas_cadastradas[analise][1]
    elif pessoas_cadastradas[analise][1] > pesado:
        pesado = pessoas_cadastradas[analise][1]
    elif pessoas_cadastradas[analise][1] < leve:
        leve = pessoas_cadastradas[analise][1]

print(f"\nQuantidade de pessoas cadastradas: {len(pessoas_cadastradas)}")
print(f"Maior Peso: {pesado}  Pessoas:/", end=' ')
for p in pessoas_cadastradas:
    if p[1] == pesado:
        print(f"{p[0]}",end=' / ')

print(f"\nMenor Peso: {leve}  Pessoas:/", end=' ')
for p in pessoas_cadastradas:
    if p[1] == leve:
        print(f"{p[0]}", end=' / ')
