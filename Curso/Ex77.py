# Crie um programa que tenha uma tupla com várias palavras (sem acentos)
# Depois mostre para cada palavra quais são as suas vogais

palavras = 'Aprender', 'Programar', 'Linguagem', 'Python', 'Curso', \
           'Grátis', 'Estudar', 'Praticar', 'Trabalha', 'Mercado', \
           'Programador', 'Futuro'

for p in palavras:
    print(f"\nNa palavra {p.upper()} temos as vogais: ", end='')
    for letra in p:
        if letra.lower() in 'aeiouáéíóúâêîôûãẽĩõũ':
            print(f"{letra}", end=' ')
