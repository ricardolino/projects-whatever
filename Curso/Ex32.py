#Descobrindo se o ano é bissexto ou não
import datetime
ano = int(input('Qual ano analisar ? Caso queira o ano atual digite 0: '))

if ano == 0:
    ano = datetime.date.today().year

if ano % 4 == 0 and (ano % 100 != 0 or ano % 400 == 0):
    print(f'Ano {ano} é Bissexto')
else:
    print(f'O ano {ano} não é Bissexto')
