#Objetivo:
#Usando o módulo math, descubra o Seno, Cosseno e a Tangênte de um ãngulo digitado pelo usuário
import math
angulo = float(input('Digite um ângulo: '))

print(f'Seno: {math.sin(math.radians(angulo)):.2f} '
      f'\nCosseno: {math.cos(math.radians(angulo)):.2f} '
      f'\nTangênte: {math.tan(math.radians(angulo)):.2f}')
