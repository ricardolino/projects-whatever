#Aplicando multa aparte da quantidade de km/h ultrapassada, cada km a mais é 7,00

velocidade = float(input('Digite a velocidade: '))

if velocidade > 80:
    print('Acima do limite permitido, você foi MULTADO')
    print(f'Multa: R${(velocidade-80) * 7:.2f}')
else:
    print('Está na velocidade permitida')
