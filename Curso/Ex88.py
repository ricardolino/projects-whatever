# Simular palpites da mega-sena
# Ler quantos jogos serão feitos e sortear 6 números de 1 a 60 para cada jogo
# Colocando cada jogo em uma lista que serão mostradas no final
# Deve mostrar cada jogo separadamente

import  random,time

jogo = []
grupo_jogos = []

qtd = int(input("Quantos jogos serão sorteados: "))

for i in range(qtd):
    ct = 0
    while True:
        num = random.randint(1, 60)
        if num not in jogo:
            ct += 1
            jogo.append(num)
        if ct == 6:
            break
    grupo_jogos.append(jogo[:])
    jogo.clear()

for i, j in enumerate(grupo_jogos):
    j.sort()
    print(f"Jogo {i+1}: {j}")
    time.sleep(1)
