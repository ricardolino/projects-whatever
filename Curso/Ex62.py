# Melhorar o Ex61
# No Ex61 ele vai mostrar o 10 primeiros termos
# Neste o programa irá perguntá quantos termos a mais o usuário quer que o programa mostre
# O programa vai parar de mostrar quando o usuário digitar  zero (0)

termo = int(input("Primeiro Termo: "))
razão = int(input("Razão: "))
mais_termos = 10
while mais_termos != 0:
    ct = 0
    while ct < mais_termos:
        print(f"{termo}", end=" - ")
        termo += razão
        ct += 1
    print("Pausa")
    mais_termos = int(input("\nQuer mostrar mais quantos termos ? "))
print("\nFim do Programa")
