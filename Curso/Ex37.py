#Converto de números inteiros para binário, octal e hexadecimal

num = int(input('Escolha m número: '))
opção = int(input('Escolha qual tipo de converção \n1 - BINÁRIO '
                  '\n2 - OCTAL \n3 - HEXADECIMAL \nSua opção é: '))

if opção == 1:
    print(f'O número {num} em BINÁRIO é {bin(num)[2:]}')
elif opção == 2:
    print(f'O número {num} em OCTAL é {oct(num)[2:]}')
elif opção == 3:
    print(f'O número {num} em HEXADECIMAL é {hex(num)[2:]}')
else:
    print('Opção Inválida')
