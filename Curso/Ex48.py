# O programa deve calcular todos números impáres que são múltiplos de três entre e 1 e 500

soma = 0
for i in range(1, 501, 2):
    if i % 3 == 0:
        soma += i
print(f'Resultado = {soma}')
