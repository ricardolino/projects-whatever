# Criar duas funções 'sorteia' e 'somaPar'
# A primeria sorteia 5 números e coloca numa lista
# A segunda soma os números pares que estavam na lista

import random

def sorteia(lista):
    for i in range(5):
        lista.append(random.randint(1, 10))
    print(f"Lista: {lista}")

def somaPar(l):
    soma = 0
    for i in l:
        if i%2 == 0:
            soma += i
    print(f"A soma dos números pares: {soma}")

números = list()
sorteia(números)
somaPar(números)
