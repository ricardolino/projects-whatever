# Criar função chamada área, que recebe as dimensões de um terreno (largura e comprimento) e mostra a área

def área(l, c):
    print(f"Área: {l*c:.1f}m²")

if __name__ == '__main__':
    largura = float(input("Largura (m): "))
    comprimento = float(input("Comprimento (m): "))
    área(largura, comprimento)
