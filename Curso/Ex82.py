# Ler valores e coloca-los numa lista
# Depois crie duas listas extras que vão conter apenas os valores pares e impares
# No final mostre as três listas

lista = []
lista_par = []
lista_impar = []

print("Digite zero [0] para sair")
while True:
    num = int(input("Digite um número: "))
    if num == 0:
        break
    lista.append(num)

for i in lista:
    if i%2 == 0:
        lista_par.append(i)
    else:
        lista_impar.append(i)

print(f"\nLista completa: {lista} \nLista impar; {lista_impar} \nLista par: {lista_par}")
