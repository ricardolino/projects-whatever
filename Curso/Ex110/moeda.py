def resumo(valor, p_a, p_d):
    print("-"*30)
    print(f"{'Resumo de Valor':^30}")
    print("-"*30)
    print(f"Metade: {moeda(metade(valor)):>21}"
          f"\nDobro: {moeda(dobro(valor)):>22}"
          f"\nAumentando {p_a}%: {moeda(aumentar(valor, p_a)):>13}"
          f"\nDiminuindo {p_d}%: {moeda(diminuir(valor, p_d)):>13}")


def metade(valor=0):
    return valor / 2


def dobro(valor=0):
    return valor * 2


def aumentar(valor=0, p=0):
    return valor + (valor * (p / 100))


def diminuir(valor=0, p=0):
    return valor - (valor * (p / 100))


def moeda(valor=0, moeda='R$'):
    return f"{moeda}{valor:.2f}".replace('.', ',')
