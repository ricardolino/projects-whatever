# Faça um lista que leia 5 valores
# Depois mostre o maior e menor número dentro desta lista e a posição destes números
# Caso o maior ou menor número apareçam duas vezes na lista deve mostrar as duas posições da sua aparição

lista = []

for i in range(5):
    lista.append(int(input("Digite: ")))

print("\n", lista)

print(f"Maior: {max(lista)}  Posição: ", end=' ')
for pos, analise in enumerate(lista):
    if analise == max(lista):
        print(f"{pos}", end='... ')

print(f"\nMenor: {min(lista)}  Posição: ", end=' ')
for analise in range(0, len(lista)):
    if lista[analise] == min(lista):
        print(f"{analise}", end='... ')
