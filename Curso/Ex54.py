# Descobrir quantas pessoas maiores de idade tem na lista e anos de nascimentos digitada pelo usuário
# A maioridade vai ser dada a pessoas com mais de 21 anos

import datetime

ct_menor = 0
ct_maior = 0
for i in range(0, 7):
    ano_nascimento: int = int(input(f'Ano de nascimento da {i+1}° Pessoa: '))
    if datetime.date.today().year - ano_nascimento < 21:
        ct_menor += 1
    else:
        ct_maior += 1

print(f'{ct_menor} pessoa(s) ainda não atigiu/atingiram a maioridade '
      f'\n{ct_maior} pessoa(s) já atigiu/atigiram a maioridade')

