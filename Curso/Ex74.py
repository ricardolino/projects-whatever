# Criar um programa que gerar cinco números aleatórios e coloca-los numa tupla
# O programa deve mostrar:
# A listagem de números
# O maior e o menor número na tupla

import  random

n_aleatórios = random.randint(0, 100), random.randint(0, 100), random.randint(0, 100), random.randint(0,100), random.randint(0, 100)

# maior = n_aleatórios[1]
# menor = n_aleatórios[1]
for i in n_aleatórios:
#     if i > maior:
#         maior = i
#     if i < menor:
#         menor = i
     print(f"{i}", end=' ')
#
# print(f"\nMaior: {maior}   Menor: {menor}")

# Outra forma de fazer usando métodos especificos para tuplas
print(f"\nMaior: {max(n_aleatórios)}   Menor: {min(n_aleatórios)}")