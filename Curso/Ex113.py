def leiaInt(msg):
    while True:
        try:
            num = input(msg).strip()
            int(num)
        except (ValueError, TypeError):
            print("ERRO! Digite um número Inteiro")
            continue
        except (KeyboardInterrupt):
            print()
            return 0
        else:
            return num

def leiaFloat(msg):
    while True:
        try:
            num = input(msg).strip()
            float(num)
        except (ValueError, TypeError):
            print("ERRO! Digite um número Real")
            continue
        except (KeyboardInterrupt):
            print()
            return 0
        else:
            return num


a = leiaInt("Digite um número Inteiro: ")
b = leiaFloat("Digite um número Real: ")
print(f"O número inteiro digitado foi {a}"
      f"\nO número real digitado foi {b}")
