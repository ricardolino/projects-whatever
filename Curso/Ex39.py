import datetime

ano_nascimento = int(input('Digite o ano do seu nascimento: '))

idade = datetime.date.today().year - ano_nascimento

if idade < 18:
    print(f'Você ainda vai se alistar no serviço militar'
          f'\nFalta(m) {18 - idade} ano(s) para você se alistar'
          f'\nVocê vai ter que se alistar em {datetime.date.today().year + (18 - idade)}')
elif idade == 18:
    print('Você precisa se alistar IMEDIATAMENTE')
else:
    print(f'Já passou do tempo de se alistar '
          f'\nJá passou {idade - 18} ano(s) do prazo de você se alistar'
          f'\nVocê devia ter se alistado em {datetime.date.today().year - (idade - 18)}')


