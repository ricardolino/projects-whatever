# Recebe a frase, manda para a função 'escreva'
# Dentro a função a mensagem deve aparecer entre duas linhas
# As linhas devem adaptar o seu tamanho de acordo com o tamanho da frase

def escreva(f):
    tamanho = len(f) + 2
    print("-" * tamanho)
    print(f"{f:^{tamanho}}")
    print("-" * tamanho)

if __name__ == '__main__':
    frase = input("Digite a frase:\n").strip()
    escreva(frase)
