# ler o nome e media de um aluno, guardando junta a situação (aprovado e reprovado) dentro de um dicionário
# No final mostre o contédudo do dicionário de forma organizada

boletim = {'Nome': input("Nome: "), 'Média': float(input("Média: "))}

if boletim['Média'] >= 7:
    boletim['Situação'] = 'Aprovado'
else:
    boletim['Situação'] = 'Reprovado'

for k, v in boletim.items():
    print(f"{k}: {v}")
