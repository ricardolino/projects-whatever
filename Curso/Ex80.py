# Ler cinco valores, coloca-los numa lista já posição correta
# Sem usa a função 'sort()'
# No final, mostre a lista ordenada

lista = []

for n in range(5):
    num = int(input("\nDigite um número: "))
    if len(lista) == 0 or num > lista[-1]:
        lista.append(num)
    else:
        for i in lista:
            if num <= i:
                lista.insert(lista.index(i), num)
                break
    print(f"O número foi adicionado na posição {lista.index(num)}")

print(f"\n{lista}")
