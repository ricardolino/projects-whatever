#Faço o jogo Jokenpô

import random, time
jogada = int(input('Opções: \n1 - PEDRA \n2 - PAPEL \n3 - TESOURA'
                   '\nQual você escolhe ? '))

jogada_cpu = random.randint(1, 3)

print('JO')
time.sleep(1)
print('KEN')
time.sleep(1)
print('PÔ\n')

if jogada_cpu == 1 and jogada == 1:
    print('Computador: PEDRA \nJogador: PEDRA \nEMPATE!')
elif jogada_cpu == 1 and jogada == 2:
    print('Computador: PEDRA \nJogador: PAPEL \nVocê Venceu!')
elif jogada_cpu == 1 and jogada == 3:
    print('Computador: PEDRA \nJogador: TESOURA \nVocê Perdeu!')
elif jogada_cpu == 2 and jogada == 1:
    print('Computador: PAPEL \nJogador: PEDRA \nVocê Perdeu!')
elif jogada_cpu == 2 and jogada == 2:
    print('Computador: PAPEL \nJogador: PAPEL \nEMPATE!')
elif jogada_cpu == 2 and jogada == 3:
    print('Computador: PAPEL \nJogador: TESOURA \nVocê Venceu!')
elif jogada_cpu == 3 and jogada == 1:
    print('Computador: TESOURA \nJogador: PEDRA \nVocê Venceu!')
elif jogada_cpu == 3 and jogada == 2:
    print('Computador: TESOURA \nJogador: PAPEL \nVocê Perdeu!')
elif jogada_cpu == 3 and jogada == 3:
    print('Computador: TESOURA \nJogador: TESOURA \nEMPATE!')
else:
    print('OPÇÃO INVÁLIDA')
