# Cria uma função chamada contador, ela recebe início, fim e o passo
# Deve realizar três contagens
# De 1 até 10, de 1 em 1
# De 10 até 0, de 2 em 2
# Uma contagem personalizada pelo usuário
# Se o usuário digitar 0 o programa considera como 1
# Se o usuário fizer uma contagem regressiva não precisa ser colocado o passo negativo

import time

def contador(inicio, fim, passo):
    if passo == 0:
        passo = 1
    if passo < 0:
        passo *= -1
    if inicio < fim:
        for i in range(inicio, fim+1, passo):
            print(f"{i}", end=' ', flush=True)
            time.sleep(0.3)
    else:
        passo *= -1
        for i in range(inicio, fim-1, passo):
            print(f"{i}", end=' ', flush=True)
            time.sleep(0.3)
    print()


contador(1, 10, 1)
contador(10, 0, 2)
i = int(input("Início da contagem: "))
f = int(input("Fim da contagem: "))
p = int(input("Passo da contagem: "))
contador(i, f, p)
