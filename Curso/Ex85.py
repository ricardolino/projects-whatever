# Recebe 7 valores, cadastra em uma única lista que separa os valores em pares e impares
# No final, mostre os valores pares e impares em ordem crescente

valores = [[],[]]

print("Digite sete valores")
for i in range(7):
    num = int(input(f"\nDigite o {i+1}º valor: "))
    if num%2 == 0:
        valores[0].append(num)
    else:
        valores[1].append(num)
valores[0].sort()
valores[1].sort()

print(f"\nLista: {valores} \nLista números pares: {valores[0]} \nLista números impares: {valores[1]}")
