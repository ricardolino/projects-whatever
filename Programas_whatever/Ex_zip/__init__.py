"""
Você foi contratado para fazer um database dos preços
dos itens de um determinado supermercado. Para tal o
supermercado lhe forneceu um arquivo txt contendo os itens
seguidos de seus respectivos preços.
Monte um database usando o modulo shelve em que as chaves
são os itens e os valores são os preços. De uma olhada
no objeto shelve.DbfilenameShelf (use a função help), especialmente no seu
método update para facilitar o trabalho. De uma olhada no método
dict também.

Força o uso do zip
"""


def pegarDadosArq():
    """
    Pega os dados do arquivo e adioca a uma lista
    """

    try:
        file = open("Lista Supermercado.txt")
    except FileNotFoundError:
        print("Arquivo Lista Supermercado.txt não encontrado")
    else:
        lista = file.readlines()
        file.close()
        return lista


def organizarDados(lista):
    """
    Organiza dados em uma lista de tuplas com o uso do zip()
    """

    produto = []
    preço = []

    for indice in range(len(lista)):
        lista[indice] = lista[indice].replace('\n', '')
        produto.append(lista[indice].split(' - ')[0])
        preço.append(lista[indice].split(' - ')[1])

    lista_zip = zip(produto, preço)
    return lista_zip


def CriarDB(lista_zip):
    """
    Cria database e adicona os dados no db
    """

    import shelve
    file = shelve.open('DBMercado.db')

    for i in lista_zip:
        file[i[0]] = i[1]

    file.close()


if __name__ == '__main__':
    CriarDB(organizarDados(pegarDadosArq()))
