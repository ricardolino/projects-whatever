import sqlite3

database = 'livraria.db'
conexao = sqlite3.connect(database) #Cria e faz a conexão com a base de dados
cur = conexao.cursor() #Faz a conexão com os comandos da base de dados

cur.execute('''create table if not exists tb_cliente (
    cpf text,
    nome text,
    idade integer)''') # faz a execução dos comandos da base de dados

conexao.commit() #Comenta/coloca na base de dados
cur.close() #Fecha a conexão com o cursor
conexao.close() #Fecha a conexão com a base de dados
