import sqlite3

def qtd_registro():
    sql = "select * from tb_cliente"
    cur.execute(sql)

    registro = cur.fetchall() #retorna a lista

    qtd = len(registro)
    return qtd

conexao = sqlite3.connect('livraria.db')
cur = conexao.cursor()

sql = 'insert into tb_cliente(cpf, nome, idade) values(?,?,?)'

lista_whatever = [('000.000.000-00', 'Rômulo', 17), ('111.111.11-11', 'Renan', 15)]

cur.executemany(sql, lista_whatever) #adiciona dados dentro de uma lista

conexao.commit()
print("Add com sucesso")
print(f"Quantidade de dados: {qtd_registro()}")

cur.close()
conexao.close()
