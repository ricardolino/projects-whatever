import sqlite3

conexao = sqlite3.connect('livraria.db')
cur  = conexao.cursor()

a = 'cpf'
sql = f"update tb_cliente set {a}=? where nome=?"

cpf1 = input("Novo CPF: ")
nome1 = input("Nome a ter CPF mudado: ")

cur.execute(sql, (cpf1, nome1))

conexao.commit()
print("Alteração feita")

cur.close()
conexao.close()
