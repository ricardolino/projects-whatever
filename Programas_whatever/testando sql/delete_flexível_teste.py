import sqlite3

def qtd_registros():
    sql = "select * from tb_cliente"
    cur.execute(sql)

    registros = cur.fetchall()

    return len(registros)


conexao = sqlite3.connect('livraria.db')
cur = conexao.cursor()
print(f"Quantidade de registros: {qtd_registros()}")
print()

sql = "delete from tb_cliente where nome=?"

nome1 = input("Nome a ser deletado: ").title()

cur.execute(sql, (nome1,))

conexao.commit()
print()
print(f"{nome1} deletado com sucesso")
print()
print(f"Quantidade de registros: {qtd_registros()}")

cur.close()
conexao.close()
