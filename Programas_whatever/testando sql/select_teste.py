import sqlite3

conexao = sqlite3.connect('livraria.db')
cur = conexao.cursor()
a = 'nome'
sql = f"select * from tb_cliente where {a}=?" #Seleciona os atributos de uma determinada tabela

cur.execute(sql, ('Ricardo', ))

registros = cur.fetchall()

if len(registros) > 0:
    for i in registros:
        print(f"CPF: {i[0]}"
              f"\nNome: {i[1]}"
              f"\nIdade: {i[2]}")
        print()
print()
print("Lista gerada com sucesso")

cur.close()
conexao.close()
