import threading
import random
import csv

class Máquina(threading.Thread):
    def __init__(self, id):
        self.id = id
        self.Somatempo = 0
        self.lock = threading.Lock()
        threading.Thread.__init__(self)

    def run(self):
        while self.Somatempo < (60*18):
            with self.lock:
                tempo = random.choice([2, 3, 4, 5, 6, 7])

                qualidade = random.choice([True, True, True, True, True, True, True, True, False, False])
                if qualidade or tempo >= 4:
                    qualidade = 1
                else:
                    qualidade = 0

                with open('dados.csv', 'a+') as arq:
                    csv.writer(arq).writerow((self.id, tempo, qualidade))

                self.Somatempo += tempo


if __name__ == '__main__':
    for i in range(3):
        m = Máquina(i)
        m.start()
        m.join()
