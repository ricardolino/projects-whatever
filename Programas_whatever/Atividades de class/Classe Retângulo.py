class Retângulo(object):
    def __init__(self, la, lb):
        self.ladoA = la
        self.ladoB = lb

    def mudar_valor(self, valorA, valorB):
        self.ladoA = valorA
        self.ladoB = valorB

    def retornar_valor(self):
        return self.ladoA, self.ladoB

    def calcular_area(self):
        return self.ladoA * self.ladoB

    def calcular_perimetro(self):
        return 2*(self.ladoA + self.ladoB)


if __name__ == '__main__':
    rt1 = Retângulo(int(input("Informe o comprimento: ")), int(input("Informe a largura: ")))

    print(f"Retornando valores: {rt1.retornar_valor()}")
    print(f"Area: {rt1.calcular_area()}")
    print(f"Perímetro: {rt1.calcular_perimetro()}")
    print()

    print("Mudando valores")
    rt1.mudar_valor(int(input("Informe o novo comprimento: ")), int(input("Informe a nova largura: ")))

    print(f"Retornando valores: {rt1.retornar_valor()}")
    print(f"Area: {rt1.calcular_area()}")
    print(f"Perímetro: {rt1.calcular_perimetro()}")
