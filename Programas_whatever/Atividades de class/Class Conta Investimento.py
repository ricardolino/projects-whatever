class contaInvestimento(object):
    def __init__(self, nome, numero_conta, saldo=0, taxa_juros=0):
        self.nome = nome
        self.numconta = numero_conta
        self.saldo = saldo
        self.taxajuros = taxa_juros

    def alterearNome(self, valor):
        self.nome = valor
        print("Nome alterado com sucesso")

    def deposito(self, valor):
        self.saldo += valor
        print("Deposito feito com sucesso")

    def saque(self, valor):
        if self.saldo > valor:
            self.saldo -= valor
            print("Saque feito com sucesso")
        else:
            print(f"Você não tem saldo o suficiente para fazer um saque de R${valor}")

    def adicionejuros(self):
        self.saldo += ((self.taxajuros/100)*self.saldo)
        print("Juros adicionados a sua conta")


if __name__ == '__main__':
    poupanca1 = contaInvestimento("Ricardo", "123-4", 1000, 10)

    for i in range(5):
        poupanca1.adicionejuros()
        print()
        print(f"Nome: {poupanca1.nome}  Conta: {poupanca1.numconta} \nSaldo: {poupanca1.saldo}")
        print()
