class Banco(object):
    __total = 10000
    TaxaReserva = 0.1
    __reservaExigida = __total * TaxaReserva

    def podeFazerEmprestimo(self, valor):
        if self.__total - valor > self.__reservaExigida:
            return True
        return False

    def MudaTotal(self, valor):
        self.__total += valor


class Conta(Banco):
    def __init__(self, s, ID, se):
        self.__saldo = s
        self.__id = ID
        self.__senha = se

    def deposito(self, senha, valor):
        if self.__senha == senha:
            self.__saldo += valor
            super(Conta, self).MudaTotal(valor)
        else:
            print("Senha incorreta")

    def saque(self, senha, valor):
        if self.__senha == senha:
            self.__saldo -= valor
            super(Conta, self).MudaTotal(-valor)
        else:
            print("Senha imcorreta")

    def podeReceberEmprestimo(self, valor):
        return super(Conta, self).podeFazerEmprestimo(valor)

    def saldo(self):
        return float(self.__saldo)


if __name__ == '__main__':
    Ricardo = Conta(0, '123-4', 123)

    Ricardo.deposito(123, 1000)
    print(f"Saldo: R${Ricardo.saldo()}")

    Ricardo.saque(123, 100)
    print(f"Saldo: R${Ricardo.saldo()}")

    print(Ricardo.podeReceberEmprestimo(1000))
