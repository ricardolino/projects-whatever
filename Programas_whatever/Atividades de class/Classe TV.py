class TV(object):
    def __init__(self, nome_canais, canal=0, volume=0):
        self.canal = canal
        self.vol = volume
        self.nome_canais = nome_canais

    def mostrar_canais(self):
        print("Use o índice de números para mudar de canal: ")
        for num, nome in enumerate(self.nome_canais):
            print(f"{num + 1} - {nome}")
        print()

    def mudarCanal(self, valor):
        self.canal = valor - 1

    def mudarVolume(self, valor):
        self.vol = valor


if __name__ == '__main__':
    lista_canais = ['Globo', 'SBT', 'Record', 'Band', 'ESPN', 'FOX', 'TNT', 'Sportv', 'Multshow', 'Rede TV']

    tv1 = TV(lista_canais)

    while True:
        print()
        controle = int(input("Digite 1 para mudar de canal e 2 para mudar volume: "))
        print()
        if controle == 1:
            tv1.mostrar_canais()
            while True:
                num = int(input("Digite o número do canal: "))
                if 0 < num <= len(lista_canais):
                    tv1.mudarCanal(num)
                    break
                else:
                    print(f"Digite um número entre 1 e {len(lista_canais)}")
            print()
            print(f"Você está no canal {num} - {lista_canais[tv1.canal]}")
            print()
        elif controle == 2:
            while True:
                num2 = int(input("Digite o volume entre 0 e 10: "))
                if 0 <= num2 <= 10:
                    tv1.mudarVolume(num2)
                    break
                else:
                    print("Digite um número entre 0 e 10")
            print()
            print(f"Volume: {tv1.vol}")
        else:
            print("Televisando desligada!")
            break
