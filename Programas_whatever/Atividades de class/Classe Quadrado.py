class Quadrado(object):
    def __init__(self, tamanho_lado):
        self.tamLado = tamanho_lado

    def mudarValorLado(self, valor):
        self.tamLado = valor

    def retornarValorLado(self):
        return self.tamLado

    def calculaArea(self):
        return self.tamLado ** 2


if __name__ == '__main__':
    q1 = Quadrado(5)

    print(f"Valor dos lados: {q1.retornarValorLado()}")
    print(f"Área do quadrado: {q1.calculaArea()}")
    print()

    q1.mudarValorLado(7)
    print(f"Valor dos lados: {q1.retornarValorLado()}")
    print(f"Área do quadrado: {q1.calculaArea()}")