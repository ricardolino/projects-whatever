class Ponto(object):
    def __init__(self):
        self.x = int(input("Digite o x: "))
        self.y = int(input("Digite o y: "))

class Retângulo(object):
    def __init__(self):
        self.largura = int(input("Digite a largura: "))
        self.altura = int(input("Digite a altura: "))
        self.ponto = Ponto()

    def localização(self):
        loc = (self.ponto.x+self.largura, self.ponto.y+self.altura)
        print(f"O outro extremo do retângulo está localizado em {loc}")

def imprimir_valores_ponto(obj):
    print(f"Valor x: {obj.ponto.x} \nValor y: {obj.ponto.y}")

def encontrar_centro(obj):
    print(f"O centro do retângulo é nas condernadas: {obj.ponto.x+(obj.largura//2)}-{obj.ponto.y+(obj.altura//2)}")
    obj.ponto.x = obj.ponto.x+(obj.largura//2)
    obj.ponto.y = obj.ponto.y+(obj.altura//2)


if __name__ == '__main__':

    rt1 = Retângulo()

    print()
    imprimir_valores_ponto(rt1)

    print()
    rt1.localização()

    print()
    encontrar_centro(rt1)

    print()
    imprimir_valores_ponto(rt1)
