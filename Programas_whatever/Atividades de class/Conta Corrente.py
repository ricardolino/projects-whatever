class contacorretente(object):
    def __init__(self, nome, numero, saldo):
        self.__a = nome
        self.__b = numero
        self.__c = saldo

    def set_a(self, valor_recebido):
        self.__a = valor_recebido
    def get_a(self):
        return self.__a

    def set_b(self, valor_recebido):
        self.__b = valor_recebido
    def get_b(self):
        return self.__b

    def set_c(self, valor_recebido):
        self.__c = valor_recebido
    def get_c(self):
        return self.__c

    def deposito (self, valor_recebido):
        self.__c = self.__c + valor_recebido

    def saque (self, valor_recebido):
        if valor_recebido <= self.__c:
            self.__c = self.__c - valor_recebido
        else:
            print("\nSaque maior que o saldo\n")

if __name__ == '__main__':
    #conta1 = contacorretente('João', '123-4', 1000.00)

    qtd_contas_serão_criadas = int(input('Quantas contas serão criadas: '))
    vetor_conta = [0]*qtd_contas_serão_criadas
    for i in range(0, len(vetor_conta)):
        vetor_conta[i] = contacorretente(input('Nome: '), input('Número da conta: '), 0)

    deposito2 = int(input('Deseja fazer deposito ?\n Sim[1] ou Não[2]: '))
    if deposito2 == 1:
        qual_conta = int(input('Qual conta: '))
        vetor_conta[qual_conta].deposito(int(input('Valor')))
    elif deposito2 == 2:
        print('Operação finalizada')
    else:
        print('ERRO')

    saque2 = int(input('Deseja fazer saque ?\n Sim[1] ou Não[2]:'))
    if saque2 == 1:
        qual_conta = int(input('Qual conta: '))
        vetor_conta[qual_conta].saque(int(input('Valor do saque: ')))
    elif saque2 == 2:
        print('Operação finalizada')
    else:
        print('ERRO')

    mostrar_conta = int(input('Mostar conta ?\nSim[1] ou Não[2]: '))
    if mostrar_conta == 1:
        qual_conta = int(input('Qual conta: '))
        print(f'Nome: {vetor_conta[qual_conta].get_a()}')
        print(f'Número da conta: {vetor_conta[qual_conta].get_b()}')
        print(f'Saldo: {vetor_conta[qual_conta].get_c()}')
    elif mostrar_conta == 2:
        print('Operação finalizada')
    else:
        print('ERRO')
    #conta1.set_b(input("Número da conta: "))
    #conta1.deposito(int(input("Deposito: ")))
    #conta1.saque(int(input("Saque: ")))
    #print(f'Titular: {conta1.get_a()}')
    #print(f'Número da conta: {conta1.get_b()}')
    #print(f'Saldo: {conta1.get_c()}')

    #conta2 = contacorretente('Eduardo', '456-7', 2000000.00)
    #print(f'Titular {conta2.get_a()}\nNúmero da conta: {conta2.get_b()}\nSaldo: {conta2.get_c()}')



