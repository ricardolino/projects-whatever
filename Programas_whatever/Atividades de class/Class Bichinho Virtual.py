"""
Classe Bichinho Virtual:Crie uma classe que modele um Tamagushi (Bichinho Eletrônico):
Atributos: Nome, Fome, Saúde e Idade b;
Métodos: Alterar Nome, Fome, Saúde e Idade;
Retornar Nome, Fome, Saúde e Idade.

Obs: Existe mais uma informação que devemos levar em consideração, o Humor do nosso tamagushi,
este humor é uma combinação entre os atributos Fome e Saúde, ou seja, um campo calculado, então não devemos
criar um atributo para armazenar esta informação por que ela pode ser calculada a qualquer momento.

Permita que o usuário especifique quanto de comida ele fornece ao bichinho e por quanto
tempo ele brinca com o bichinho. Faça com que estes valores afetem quão rapidamente os níveis de fome e tédio caem.

Crie uma "porta escondida" no programa do programa do bichinho virtual que mostre os valores exatos dos atributos do
objeto. Consiga isto mostrando o objeto quando uma opção secreta, não listada no menu, for informada na escolha do
usuário. Dica: acrescente um método especial str() à classe Bichinho.

Crie uma Fazenda de Bichinhos instanciando vários objetos bichinho e mantendo o controle deles através de uma lista.
Imite o funcionamento do programa básico, mas ao invés de exigis que o usuário tome conta de um único bichinho,
exija que ele tome conta da fazenda inteira. Cada opção do menu deveria permitir que o usuário executasse uma ação
para todos os bichinhos (alimentar todos os bichinhos, brincar com todos os bichinhos, ou ouvir a todos os
bichinhos). Para tornar o programa mais interessante, dê para cada bichinho um nivel inicial aleatório de fome e
tédio.
"""


class BichinhoVirtual(object):
    def __init__(self, nome, alimento=0, saude=0, idade=1):
        self.__nome = nome
        self.__alimento = alimento
        self.__saude = saude
        self.__idade = idade

    def __str__(self):
        return f"Nome: {self.get_nome()}" \
               f"\nIdade: {self.get_idade()}" \
               f"\nSaúde: {self.__saude}/10 - {self.get_saude()}" \
               f"\nApetite: {self.__alimento}/10 - {self.get_alimento()}"

    def set_nome(self, valor):
        self.__nome = valor

    def get_nome(self):
        return self.__nome

    def set_alimento(self, valor):
        if valor >= 10:
            self.__alimento = 10
        else:
            self.__alimento += valor

    def get_alimento(self):
        if self.__alimento >= 5:
            return 'Satisfeito'
        else:
            return 'Fome'

    def set_saude(self):
        if self.__saude >= 10:
            self.__saude = 10
        else:
            self.__saude += 1

    def get_saude(self):
        if self.__saude >= 5:
            return 'Boa'
        else:
            return 'Ruim'

    def set_idade(self):
        self.__idade += 1
        self.__saude -= 1

    def get_idade(self):
        return self.__idade

    def humor(self, valor=0):
        num = (self.__alimento + self.__saude + valor) // 2
        if num >= 5:
            return 'Animado'
        else:
            return 'Tédio'

    def brincar(self, valor):
        if valor == 1:
            self.humor(1)
            self.__alimento -= 1
        elif 10 > valor >= 5:
            self.humor(valor)
            self.__alimento -= valor
            self.__saude -= 1
        elif valor >= 10:
            self.humor(10)
            self.__alimento -= 10
            self.__saude -= 2


if __name__ == '__main__':
    bichinho1 = BichinhoVirtual('Tamagushi')

    ct = 0
    while True:

        ct += 1
        if ct % 7 == 0:
            bichinho1.set_idade()

        print()
        print(
            f"Nome: {bichinho1.get_nome()} \nIdade: {bichinho1.get_idade()} ano"
            f"\nSaúde: {bichinho1.get_saude()} "
            f"\nApetite: {bichinho1.get_alimento()} "
            f"\nHumor: {bichinho1.humor()}")

        print()
        alteracao = 0
        while alteracao != 1 and alteracao != 2:
            alteracao = int(input("1 - SIM\n2 - NÃO\nDeseja fazer alteração: "))

        print()
        if alteracao == 1:

            opcao = 0
            while opcao != 1 and opcao != 2 and opcao != 3 and opcao != 4 and opcao != 5 and opcao != 555:
                opcao = int(input("1 - Mudar nome\n2 - Melhorar saúde\n3 - Dar comida\n4 - Adiciona idade\n5 - "
                                  "Brincar\nSeleciona a opção: "))
            print()

            if opcao == 1:
                bichinho1.set_nome(input("Mudar nome: "))
            elif opcao == 2:
                bichinho1.set_saude()
            elif opcao == 3:
                bichinho1.set_alimento(int(input("Quanto de comida: ")))
            elif opcao == 4:
                bichinho1.set_idade()
            elif opcao == 5:
                bichinho1.brincar(int(input("O quanto vc quer brincar com o bichinho? ")))
            else:
                print(bichinho1)
        else:
            break
