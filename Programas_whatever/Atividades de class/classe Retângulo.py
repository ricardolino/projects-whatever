class Retangulo(object):
    def __init__(self, base, altura):
        self.ladoA = altura
        self.ladoB = base

    def mudarValorLados(self, valorA, valorB):
        self.ladoA = valorA
        self.ladoB = valorB

    def retornaValorLados(self):
        return self.ladoA, self.ladoB

    def calcularArea(self):
        return self.ladoA * self.ladoB

    def calcularPerimetro(self):
        return (self.ladoA*2) + (self.ladoB*2)


if __name__ == '__main__':
    local1 = Retangulo(float(input("Tamanho da base: ")), float(input("Tamanho da altura: ")))

    tamPisosRodape = float(input("Informe o tamanho dos Pidos de Rodapé: "))

    if local1.calcularPerimetro() % tamPisosRodape != 0:
        qtdPisosRodape = (local1.calcularPerimetro()//tamPisosRodape) + 1
    else:
        qtdPisosRodape = local1.calcularPerimetro() // tamPisosRodape

    print()
    print(f"Valores do local1 [Altura/Base]: {local1.retornaValorLados()}")
    print(f"Area do local1: {local1.calcularArea()}")
    print(f"Perímetro do local1: {local1.calcularPerimetro()}")
    print(f"Quantidade de Pisos de Rodapé a serem comprados: {qtdPisosRodape}")
    print()

    local1.mudarValorLados(float(input("Tamanho da altura: ")), float(input("Tamanho da base: ")))

    if local1.calcularPerimetro()%tamPisosRodape != 0:
        qtdPisosRodape = (local1.calcularPerimetro()//tamPisosRodape) + 1
    else:
        qtdPisosRodape = local1.calcularPerimetro() // tamPisosRodape

    print()
    print(f"Valores do local1 [Altura/Base]: {local1.retornaValorLados()}")
    print(f"Area do local1: {local1.calcularArea()}")
    print(f"Perímetro do local1: {local1.calcularPerimetro()}")
    print(f"Quantidade de Pisos de Rodapé a serem comprados: {qtdPisosRodape}")
    print()