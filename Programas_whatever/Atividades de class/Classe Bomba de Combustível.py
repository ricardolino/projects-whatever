class bombaCombustível(object):
    def __init__(self, tipoCombustivel, valorLitro, quantidadeCombustivel):
        self.tipoCombustivel = tipoCombustivel
        self.valorLitro = valorLitro
        self.qtdCombustivel = quantidadeCombustivel

    def abastecerPorValor(self, valor):
        if self.qtdCombustivel - (valor / self.valorLitro) > 0:
            print(f"Você abasteceu {valor / self.valorLitro} litros de {self.tipoCombustivel} no valor de R${valor}")
            self.alterarQuantidadeCombustivel(valor / self.valorLitro)
        else:
            print("Não há combustivel o suficiente")

    def abastecerPorLitro(self, litros):
        if self.qtdCombustivel - litros > 0:
            print(f"Você abasteceu {litros} litros de {self.tipoCombustivel} no valor de R${litros * self.valorLitro}")
            self.alterarQuantidadeCombustivel(litros)
        else:
            print("Não há combustivel o suficiente")

    def alterarValor(self, valor):
        self.valorLitro = valor

    def alterarCombustivel(self, tipo):
        self.tipoCombustivel = tipo

    def alterarQuantidadeCombustivel(self, litros=0):
        self.qtdCombustivel = self.qtdCombustivel - litros
        print(f"Quantidade de litros na bomba: {self.qtdCombustivel}")


if __name__ == '__main__':
    bomba1 = bombaCombustível("Gasolina", 5.00, 100)

    bomba1.abastecerPorValor(50)
    print()

    bomba1.abastecerPorLitro(20)
    print()

    bomba1.alterarValor(4)

    bomba1.abastecerPorValor(50)
    print()

    bomba1.abastecerPorLitro(20)
    print()

    bomba1.alterarCombustivel('Disel')

    bomba1.abastecerPorValor(50)
    print()

    bomba1.abastecerPorLitro(20)
    print()

    bomba1.abastecerPorValor(50)
    print()
