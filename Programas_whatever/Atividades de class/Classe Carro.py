class Carro(object):
    def __init__(self, consumo, taque=0):
        self.consumo = consumo
        self.taque = taque

    def andar(self, valor):
        if self.taque - (valor / self.consumo) > 0:
            self.taque -= (valor / self.consumo)
            print(f"Você andou {valor}Km")
        else:
            print(f"Você não tem combustivel o suficiente para andar {valor}Km")

    def obterGasolina(self):
        print(f"Nível gasolina: {self.taque}")

    def adicionarGasolina(self, valor):
        self.taque += valor
        print(f"{valor} litros de gasolina adicionados")


if __name__ == '__main__':
    Fusca = Carro(15)

    Fusca.adicionarGasolina(20)

    Fusca.andar(100)

    Fusca.obterGasolina()
