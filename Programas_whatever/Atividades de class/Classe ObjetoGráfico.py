class ObjetpGrafico(object):
    def __init__(self, c_p, p, c_c):
        self.cor_de_preenchimento = c_p
        self.preenchimento = p
        self.cor_de_contorno = c_c


class Retangulo(ObjetpGrafico):
    def __init__(self, b, a, c_p, p, c_c):
        self.base = b
        self.altura = a
        super().__init__(c_p, p, c_c)

    def perimentro(self):
        print(f"Perimetro do retângulo: {(self.base*2)+(self.altura*2)}")

    def area(self):
        print(f"Area do retâmgulo: {self.base*self.altura}")


class Circulo(ObjetpGrafico):
    def __init__(self, r, c_p, p, c_c):
        self.raio = r
        super().__init__(c_p, p, c_c)

    def perimetro(self):
        print(f"Perímetro do circulo: {2*3.14*self.raio}")

    def area(self):
        print(f"Area do circulo: {3.14*(self.raio**2)}")


class Triangulo(ObjetpGrafico):
    def __init__(self, b, a, c_p, p, c_c):
        self.base = b
        self.altura = a
        super().__init__(c_p, p, c_c)

    def perimetro(self):
        print(f"Perimetro do triângulo: {self.base*3}")

    def area(self):
        print(f"Area do triângulo: {(self.base*self.altura)/2}")


if __name__ == '__main__':
    rt = Retangulo(10, 5, 2, True, 3)
    c = Circulo(2, 5, True, 6)
    tr = Triangulo(6, 12, 7, True,8)

    print("Retângulo")
    rt.perimentro()
    rt.area()
    print()

    print("Circulo")
    c.perimetro()
    c.area()
    print()

    print("Triangulo")
    tr.perimetro()
    tr.area()
    print()
