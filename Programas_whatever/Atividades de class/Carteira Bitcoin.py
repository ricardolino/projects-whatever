
#Carteira covertedora de dinheiro (no caso somente Bitcoin, Dolar e Real), você esclhe qual dinheiro você quer em sua
#carteira (detale você só escolher um por conta) e passa a deposita o dinherio que você quer converte.

class carteiravirtual(object):
    def __init__(self, nome, cpf, ncelular, nconta, saldo, tipoconta):
        self.__n = nome
        self.__c = cpf
        self.__ncel = ncelular
        self.__ncont = nconta
        self.__s = saldo
        self.__tipo = tipoconta

    def set_n(self, valor):
        self.__n = valor
    def get_n(self):
        return self.__n

    def set_c(self, valor):
        self.__c = valor
    def get_c(self):
        return self.__c

    def set_ncel(self, valor):
        self.__ncel = valor
    def get_ncel(self):
        return self.__ncel

    def set_ncont(self, valor):
        self.__ncont = valor
    def get_ncont(self):
        return self.__ncont

    def set_s(self, valor):
        self.__s  = valor
    def get_s(self):
        return self.__s

    def get_tipo(self):
        return self.__tipo

    def deposito_real_bitcoin(self, valor):
        self.__s = 0.000028 * valor

    def deposito_real_dolar(self, valor):
        self.__s = 0.25 * valor

    def deposito_dolar_bitcoin(self, valor):
        self.__s = 0.00011 * valor

    def deposito_dolar_real(self, valor):
        self.__s = 3.99 * valor

    def deposito_bitcoin_real(self, valor):
        self.__s = 36387.24 * valor

    def deposito_bitcoin_dolar(self, valor):
        self.__s = 9136.98 * valor

    def saque(self, valor):
        if valor > self.__s:
            print('Saldo insuficiente')
        else:
            self.__s = self.__s - valor

if __name__ == '__main__':
    qtd = int(input('Quantas contas serão criadas: '))
    dados = [0] * qtd

    for i in range(0, len(dados)):
        dados[i] = carteiravirtual(input('Nome: '), input('CPF: '), input('Número de celular: '), i + 1, 0,
                        int(input('\nUse os indices de números para seguir no programa '
                                  '\n1 - Bitcoin \n2 - Dólar \n3 - Real \nVocê deseja ter que tipo de dinhero em sua carteira ?')))
        print(f'\nConta feita com sucesso \n\n\nRegras: Você só pode ter na sua carteira um tipo de dinherio \n\n\nInformações: '
        f'\nNome: {dados[i].get_n()} \nCPF: {dados[i].get_c()} \nCelular: {dados[i].get_ncel()} \nNúmero da conta: {i + 1}')

    alteracao = 0
    while(alteracao != 1 or alteracao != 2):
        alteracao = int(input('\n\n1 - Sim \n2 - Não \nDeseja fazer alguma alteração ?'))
        if alteracao == 1 or alteracao == 2:
            break

    while(alteracao == 1):
        qual_conta = int(input('\nUse o número da conta para fazer alteração na conta que quiser\nQual conta alterar: '))
        for i in range(0, len(dados)):
            if i == qual_conta - 1:
                tipo_de_alteracao = int(input('\n1 - Nome \n2 - CPF \n3 - Número de celular  \n4 - Deposito e Saque '
                                              '\nTipo de alteração: '))
                if  tipo_de_alteracao == 1:
                    dados[qual_conta - 1].set_n(input('Alterando Nome: '))
                elif tipo_de_alteracao == 2:
                    dados[qual_conta - 1].set_c(input('Alterando CPF: '))
                elif tipo_de_alteracao == 3:
                    dados[qual_conta - 1].set_ncel(input('Alterando Número de Celular: '))
                elif tipo_de_alteracao == 4:
                    if dados[qual_conta - 1].get_tipo() == 1:
                        tipo_de_alteracao = int(input('\n1 - Deposito \n2 - Saque \nTipo de alteração: '))
                        if tipo_de_alteracao == 1:
                            tipo_deposito = int(input('\n1 - Real \n2 - Dólar \nDeseja deposita o que ? '))
                            if tipo_deposito == 1:
                                dados[qual_conta - 1].deposito_real_bitcoin(float(input('Quanto deseja deposita: ')))
                                print('Deposito feito com sucesso !')
                            elif tipo_deposito == 2:
                                dados[qual_conta - 1].deposito_dolar_bitcoin(float(input('Quanto deseja deposita: ')))
                                print('Deposito feito com sucesso !')
                            else:
                                print('Caracter errado \nOperação finalizada ')
                        elif tipo_de_alteracao == 2:
                            dados[qual_conta - 1].saque(float(input('Quanto sacar: ')))
                        else:
                            print('Caracter errado \nOperação finalizada ')
                    elif dados[qual_conta - 1].get_tipo() == 2:
                        tipo_de_alteracao = int(input('\n1 - Deposito \n2 - Saque \nTipo de alteração: '))
                        if tipo_de_alteracao == 1:
                            tipo_deposito = int(input('\n1 - Real \n2 - Bitcoin \nDeseja deposita o que ? '))
                            if tipo_deposito == 1:
                                dados[qual_conta - 1].deposito_real_dolar(float(input('Quanto deseja deposita: ')))
                                print('Deposito feito com sucesso !')
                            elif tipo_deposito == 2:
                                dados[qual_conta - 1].deposito_bitcoin_dolar(float(input('Quanto deseja deposita: ')))
                                print('Deposito feito com sucesso !')
                            else:
                                print('Caracter errado \nOperação finalizada ')
                        elif tipo_de_alteracao == 2:
                            dados[qual_conta - 1].saque(float(input('Quanto sacar: ')))
                        else:
                            print('Caracter errado \nOperação finalizada ')
                    elif dados[qual_conta - 1].get_tipo() == 3:
                        tipo_de_alteracao = int(input('\n1 - Deposito \n2 - Saque \nTipo de alteração: '))
                        if tipo_de_alteracao == 1:
                            tipo_deposito = int(input('\n1 - Dolar \n2 - Bitcoin \nDeseja deposita o que ? '))
                            if tipo_deposito == 1:
                                dados[qual_conta - 1].deposito_dolar_real(float(input('Quanto deseja deposita: ')))
                                print('Deposito feito com sucesso !')
                            elif tipo_deposito == 2:
                                dados[qual_conta - 1].deposito_bitcoin_real(float(input('Quanto deseja deposita: ')))
                                print('Deposito feito com sucesso !')
                            else:
                                print('Caracter errado \nOperação finalizada ')
                        elif tipo_de_alteracao == 2:
                            dados[qual_conta - 1].saque(float(input('Quanto sacar: ')))
                        else:
                            print('Caracter errado \nOperação finalizada ')
                    else:
                        print('\nERRO\n(Vôce digitou o tipo de dinheiro em sua carteira errado)\nOperação finalizada')
                else:
                    print('\n\nERRO\n Caracter errado')
            else:
                print('\nCONTA INEXISTENTE')
        alteracao = 0
        while (alteracao != 1 or alteracao != 2):
            alteracao = int(input('\n1 - Sim \n2 - Não \nDeseja fazer alguma alteração ?'))
            if alteracao == 1 or alteracao == 2:
                break

    for i in range(0, len(dados)):
        if dados[i].get_tipo() == 1:
            print(f'\n\nNome: {dados[i].get_n()} \nCPF: {dados[i].get_c()} \nNúmero da conta: {dados[i].get_ncont()} '
              f'\nSaldo: {dados[i].get_s()}BTC')
        elif dados[i].get_tipo() == 2:
            print(f'\n\nNome: {dados[i].get_n()} \nCPF: {dados[i].get_c()} \nNúmero da conta: {dados[i].get_ncont()} '
                  f'\nSaldo: {dados[i].get_s()}$')
        elif dados[i].get_tipo() == 3:
            print(f'\n\nNome: {dados[i].get_n()} \nCPF: {dados[i].get_c()} \nNúmero da conta: {dados[i].get_ncont()} '
                  f'\nSaldo: {dados[i].get_s()}R$')
