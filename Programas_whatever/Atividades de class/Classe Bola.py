class Bola(object):
    def __init__(self, cor, circunferência, material):
        self.cor = cor
        self.circun = circunferência
        self.mat = material

    def trocaCor(self, valor):
        self.cor = valor

    def mostraCor(self):
        print(f"Cor da bola: {self.cor}")

if __name__ == '__main__':
    bl1 = Bola('Azul', 10, 'Plastico')

    bl1.mostraCor()

    bl1.trocaCor('Vermelho')

    bl1.mostraCor()
