import Conta

conta1 = Conta.Conta(123, 1000, '111.111.111-11')
conta2 = Conta.Conta(456, 500, '000.000.000-00')

print(f'Saldo conta1: {conta1.saldo}\nSaldo conta2: {conta2.saldo}')

conta1.tranferencias(conta2, 100)

print(f'Saldo conta1: {conta1.saldo}\nSaldo conta2: {conta2.saldo}')
