from Tranferencias import *


class Conta(object):
    def __init__(self, id, saldo, cpf):
        self.id = id
        self.saldo = saldo
        self.cpf = cpf

    def tranferencias(self, contaDestino, valor):
        listaThreads = []
        for i in range(valor):
            thread = Transferencia(i, valor, threading.Lock(), self, contaDestino)
            thread.start()
            listaThreads.append(thread)

        for t in listaThreads:
            t.join()

        print('Transferência finalizada')
