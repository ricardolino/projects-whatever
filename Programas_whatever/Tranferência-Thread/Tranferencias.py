import threading


class Transferencia(threading.Thread):
    def __init__(self, id, qtd, lock, conta1, conta2):
        self.id = id
        self.qtd = qtd
        self.lock = lock
        self.conta1 = conta1
        self.conta2 = conta2
        threading.Thread.__init__(self)

    def run(self):
        with self.lock:
                self.conta1.saldo -= 1
                self.conta2.saldo += 1
                #print(self.id, self.conta1.saldo, self.conta2.saldo)
