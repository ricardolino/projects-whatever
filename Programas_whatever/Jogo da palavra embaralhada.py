'''
Desenvolva um jogo em que o usuário tenha que adivinhar uma palavra que será mostrada com as letras embaralhadas.
O programa terá uma lista de palavras lidas de um arquivo texto e escolherá uma aleatoriamente.
O jogador terá seis tentativas para adivinhar a palavra.
Ao final a palavra deve ser mostrada na tela, informando se o usuário ganhou ou perdeu o jogo.
'''

import random

lista = ('Cachorro', 'Casa', 'Economia', 'Programação', 'Diversão', 'Esporte', 'Entreterimento', 'Amizade', 'Amor', 'Esperança', 'Zebra')
palavra = random.choice(lista)
letras_embaralhadas = []

for letra in palavra:
    letras_embaralhadas.append(letra)
random.shuffle(letras_embaralhadas)
palavra_embaralhada = ''.join(letras_embaralhadas)

print("Acerte qual é a palavra embaralhada. Você tem 6 tentativas")
acerto = False
for i in range(0, 6):
    print()
    print(palavra_embaralhada)
    jogada = input(f"Tentativa número {i+1}: ").strip().title()
    if jogada == palavra:
        acerto = True
        break

print()
if acerto:
    print(f"Parabens você venceu! A palavra era {palavra}")
else:
    print(f"Infelizmente você gastou todas suas tentativas, a palavra era {palavra}. \nVolte sempre!")
