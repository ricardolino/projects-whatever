import os
import encontra

def encontra_destino():
	for nomeDiretorio, ListaNomeSubdiretório, ListaNomeArquivos in os.walk('.'):
		if nomeDiretorio.split('/')[-1] == 'partição-correta':
			return nomeDiretorio + '/'


def mover_diretorio(listaCaminhoInicio, caminhoDestino):
	for caminho in listaCaminhoInicio:
		os.system(f'mv {caminho} {caminhoDestino}')


mover_diretorio(encontra.encontra_caminho(), encontra_destino())
