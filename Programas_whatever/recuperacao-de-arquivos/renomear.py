import os
import encontra

def renomear(lista):
	"""
	Muda de diretório e lista os arquivos dentro do novo diretório
	"""
	for diretorio in lista:
		os.chdir(os.path.abspath(diretorio))
		for arq in os.listdir():
			if os.path.splitext(arq)[1] == '.txt':
				novoNomeDiretorio = open(arq).readline()[:-1]
				os.chdir('..')
				os.rename(diretorio.split('/')[1], novoNomeDiretorio)
				os.chdir('..')


renomear(encontra.encontra_caminho())
