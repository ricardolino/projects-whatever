import os

def encontra_caminho():
	lista = []
	for nomeDiretorio, ListaNomeSubdiretório, ListaNomeArquivos in os.walk('.'):
		if len(ListaNomeArquivos) > 0:
			for nomeArquivo in ListaNomeArquivos:
				if '.kdenlive' in os.path.splitext(nomeArquivo):
					lista.append(os.path.normpath(nomeDiretorio))
	return lista
