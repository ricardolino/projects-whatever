"""
Utilizando o que foi feito no programa
para a função map, a USP quer que você crie um
novo programa:

Nele você pegará todos os valores corrigidos
e os valores anteriores a correção a partir dos
objetos guardados em listas.obj e deverá:

1) Pedir um valor limite de correção
2) Criar uma lista com o valor monetário da correção
3) Filtrar os valores cuja correção não ultrapassa esse valor
4) Calcular a soma de todos os valores filtrados
5) Disponibilizar para o usuário o total de correção
"""


def filtrar(valor):
    global limite_correcao
    if limite_correcao >= valor:
        return valor


valor_correcao = list(map((lambda *x: float(f'{float(x[1]) - float(x[0]):.2f}')), open('gastos.txt'), open('gastos_corrigidos.txt')))

limite_correcao = int(input("Valor limite: R$"))
valores_filtrados = list(filter(filtrar, valor_correcao))

soma_valores_filtrados = round(sum(valores_filtrados), 2)

print(f"Total da correção: {soma_valores_filtrados}")