
def criarjogo():
    lista = list(range(1, 16))
    lista.insert(0,'*')
    m = []
    import random
    for i in range(4):
        linha = []
        for j in range(4):
            n = random.choice(lista)
            linha.append(n)
            lista.remove(n)
        m.append(linha)
    return m

def jogada(m):
    from validação import validar_jogada
    posições = validar_jogada(m)
    print()
    linha_num = int(posições[0][0])
    coluna_num = int(posições[0][1])
    linha_vazio = int(posições[1][0])
    coluna_vazio = int(posições[1][1])

    m[linha_vazio][coluna_vazio] = m[linha_num][coluna_num]
    m[linha_num][coluna_num] = '*'
    return m