def encontrando_vazio(m):
    for l, c in enumerate(m):
        try:
            coluna_vazio = c.index('*')
        except:
            coluna_vazio = 0
        else:
            linha_vazio = l
            break

    posição = str(linha_vazio) + str(coluna_vazio)
    return posição

def encontrando_num(text, m):
    while True:
        num = int(input(text))
        for l,c in enumerate(m):
            try:
                coluna = c.index(num)
            except:
                coluna = ''
            else:
                linha = l
                break

        if coluna == '':
            print(f"Número {num} não existe em matriz")
        else:
            break

    posição = str(linha) + str(coluna)
    return posição


def validar_jogada(m):
    pos_vazio = encontrando_vazio(m)
    aux_vazio = int(pos_vazio)
    while True:
        pos1 = encontrando_num("Digite a posição do número a ser movimentado: ", m)
        aux_pos1 = int(pos1)
        if aux_pos1 == aux_vazio:
            print("Jogada inválida")
        elif (aux_pos1+1) == aux_vazio or (aux_pos1+10) == aux_vazio:
            break
        elif (aux_pos1-1) == aux_vazio or (aux_pos1-10) == aux_vazio:
            break
        else:
            print("Jogada inválida")
    return pos1, pos_vazio