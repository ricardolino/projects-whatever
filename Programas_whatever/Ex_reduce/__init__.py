"""
Obtenha o maior número de um arquivo com 100000 numeros
nums.txt usando o reduce

Tentar fazer em uma linha
"""


def comparar(elementoAtual, elementoNovo):
    if int(elementoNovo) > int(elementoAtual):
        return elementoNovo
    else:
        return elementoAtual


import functools

print(functools.reduce(comparar, open('nums.txt')))
print(functools.reduce(max, [n for n in open('nums.txt')]))
