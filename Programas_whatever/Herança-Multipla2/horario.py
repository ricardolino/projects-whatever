from relogio import Relogio
from calendario import Calendario

class Horario(Relogio, Calendario):
    def __init__(self,h,mi,s,d,me,a):
        Relogio.__init__(self, h, mi, s)
        Calendario.__init__(self, d,me,a)

    def __str__(self):
        return f'{Relogio.__str__(self)} - {Calendario.__str__(self)}'

    def tick(self):
        super(Horario, self).tick()
        if Relogio.__str__(self) == '00:00:00':
            super(Horario, self).avancaDia()

h = Horario(23,58,30,30,7,2021)
for i in range(92):
    print(h)
    h.tick()