class Calendario(object):
    def __init__(self,d,me,a):
        self.dia = d
        self.mes = me
        self.ano = a

    def __str__(self):
        if self.dia < 10:
            diaCorreto = f'0{self.dia}'
        else:
            diaCorreto = self.dia
        if self.mes < 10:
            mesCorreto = f'0{self.mes}'
        else:
            mesCorreto = self.mes
        return f'{diaCorreto}/{mesCorreto}/{self.ano}'

    def bissexto(self):
        if self.ano % 4 == 0 and (self.ano % 100 != 0 or self.ano % 400 == 0):
            return True
        return False

    def mesCom31Dias(self):
        lista = [1,3,5,7,8,10,12]
        if self.mes in lista:
            return True
        return False

    def avancaDia(self):
        if self.dia < 28:
            self.dia += 1
        elif self.mes == 2:
            if not self.bissexto() or self.dia == 29:
                self.dia = 1
                self.mes += 1
            else:
                self.dia += 1
        elif self.dia < 30:
            self.dia +=1
        elif not self.mesCom31Dias():
            self.dia = 1
            self.mes += 1
        else:
            if self.dia == 30:
                self.dia += 1
            else:
                self.dia = 1
                if self.mes == 12:
                    self.mes = 1
                    self.ano += 1
                else:
                    self.mes += 1
