class Relogio(object):
    def __init__(self, h,mi,s):
        self.hora = h
        self.minuto = mi
        self.segundo = s

    def __str__(self):
        if self.hora < 10:
            horaCorreta = f'0{self.hora}'
        else:
            horaCorreta = self.hora
        if self.minuto < 10:
            minutoCorreto = f'0{self.minuto}'
        else:
            minutoCorreto = self.minuto
        if self.segundo < 10:
            segundoCorreto = f'0{self.segundo}'
        else:
            segundoCorreto = self.segundo
        return f'{horaCorreta}:{minutoCorreto}:{segundoCorreto}'

    def tick(self):
        if self.segundo == 59:
            self.segundo = 0
            if self.minuto == 59:
                self.minuto = 0
                if self.hora == 23:
                    self.hora = 0
                else:
                    self.hora += 1
            else:
                self.minuto += 1
        else:
            self.segundo += 1
