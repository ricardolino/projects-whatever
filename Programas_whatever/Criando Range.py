"""
Construa o seu próprio objeto
range
"""
class meu_range(object):
    def __init__(self, *valores):
        if len(valores) == 0:
            raise TypeError("range necessita de pelo menos um valor")
        elif len(valores) == 1:
            self.com = 0
            self.fim = valores[0]
            self.passo = 1
        elif len(valores) == 2:
            self.com = valores[0]
            self.fim = valores[1]
            if self.com > self.fim:
                self.passo = -1
            else:
                self.passo = 1
        elif len(valores) == 3:
            self.com = valores[0]
            self.fim = valores[1]
            self.passo = valores[2]

        try:
            assert type(self.com) == int
            assert type(self.fim) == int
            assert type(self.passo) == int
        except AssertionError:
            raise TypeError("Os valores dentro do range necessitam ser interios")

        try:
            assert self.passo != 0
        except AssertionError:
            raise TypeError("O passo não pode ser igual a zero")

    def __str__(self):
        return f"range({self.com}, {self.fim}, {self.passo})"

    def __iter__(self):
        return self

    def __next__(self):
        if (self.com != self.fim):
            retorno = self.com
            self.com += self.passo
            return retorno
        else:
            raise StopIteration

# TESTES
testes = [(5,), (1.7,), ("opa",), (), (1, 5, 0), (1, 5), (5, 2), (5, 2, -1)]

for t in testes:
    print(f"Teste: FOR I IN RANGE{str(t)}")

    try:
        print("Resultados da iteração:", end=" ")
        for i in meu_range(*t):
            print(i, end=" ")

        print(f"\nObjeto range: {meu_range(*t)}")

        print(f"Iterador: {iter(meu_range(*t))}")

    except (TypeError, ValueError) as e:
        print(e)

    print()


