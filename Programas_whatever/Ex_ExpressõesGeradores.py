"""
Escreva um simples programa que receba
uma palavra e forneça todas as permutações
possíveis da palavra.
Faça isso a partir de expressões
geradoras
"""

palavra = input("Digite a palavra: ")

gerador = (palavra[i:] + palavra[:i] for i in range(len(palavra)))

for i in range(len(palavra)):
    print(next(gerador))
