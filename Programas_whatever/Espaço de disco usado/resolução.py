def converção(bytes):
    """
    :param bytes: Recebe a quantidade de bytes
    :return: Uma string de duas casas decimais com o valor convertido em megabytes
    """

    return float(f'{int(bytes) / 1048576:.2f}')


def lendo_arquivo_entrada():
    """
    Ler o arquivo de entrada.
    Adicona cada pessoa e e megabytes usados em uma lista filha
    Devolve uma lista matriz com as listas filhas
    """

    try:
        arquivo = open('arquivo de entrada.txt', 'r')
    except:
        print("Erro ao abri arquivo")
        arquivo.close()
        return

    dados = []
    pessoas = []

    while True:

        a = arquivo.readline(15).strip()
        if a == '':
            break
        b = converção(arquivo.readline().strip())

        pessoas.append(a)
        pessoas.append(b)
        dados.append(pessoas.copy())
        pessoas.clear()

    arquivo.close()

    return dados


def calculo(dados):
    """
    :param dados: Recebe a lista com os dados

    :return: Total, média de megabytes usados, e a lista com a porcetagem
    megabytes usado por cada pessoa dentro da respectiva lista filha
    """

    total = 0
    for i in dados:
        total += i[1]

    for j in range(len(dados)):
        dados[j].append(float(f'{100 * dados[j][1] / total:.2f}'))

    media = float(f'{total / len(dados):.2f}')

    return total, media, dados


def ordenar_dados(dados):
    """
    :param dados: Recebe a lista com dados de todos os usuários da memoria
    :return: A lista ordenada de acordo com a quantodade de memória usada
    """

    copia_dados = []

    for i in dados:
        if len(copia_dados) > 0:
            j = 0
            nao_add = False
            while True:
                if i[1] > copia_dados[j][1]:
                    copia_dados.insert(j, i[:])
                    break
                j += 1
                if j == len(copia_dados):
                    nao_add = True
                    break
            if nao_add:
                copia_dados.append(i[:])
        else:
            copia_dados.append(i[:])

    return copia_dados


def criar_relatorio():
    """
    Cria arquivo e escreve o menu
    :return: None
    """
    try:
        arquivo = open('Relatório.txt', 'w')
    except:
        print("Erro ao criar relatório")
        arquivo.close()

    escrito = f"{'ACME Inc.':<24}{'Uso do espaço em disco pelos usuários'}\n"
    linha = '-' * 60
    escrito2 = f"\n{'Nr.':<5}{'Usuário':<15}{'Espaço utilizado':<20}{'% de uso':>9}\n\n"

    try:
        arquivo.write(escrito)
        arquivo.write(linha)
        arquivo.write(escrito2)
    except:
        print("Erro ao escrever menu Relatório.txt")
        arquivo.close()
        return

    return


def adicionando_dados_relatorio(dados, total, media, qtd):
    """
    Adicona os dados ao Relatório.txt
    :param dados: Lista com os dados de cada pessoa
    :param total: Total de memória usado
    :param media: Média de memória usado por pessoa
    :return: None
    """

    try:
        arquivo = open('Relatório.txt', 'a')
    except:
        print("Erro ao abrir arquivo")
        arquivo.close()
        return

    if qtd > len(dados):
        qtd = len(dados)

    for i in range(qtd):
        escrito = f"{i + 1:<5}{dados[i][0]:<15}{dados[i][1]:<20}{dados[i][2]:>9}\n"

        try:
            arquivo.write(escrito)
        except:
            print("Erro a adiciona dados ao Relatório.txt")
            arquivo.close()
            return

    escrito_total = f"\nEspaço total ocupado: {total} MB\n"
    escrito_media = f"Espaço médio ocupado: {media} MB"

    try:
        arquivo.write(escrito_total)
        arquivo.write(escrito_media)
    except:
        print("Erro a adiciona dados ao Relatório.txt")
        arquivo.close()
        return

    arquivo.close()

    return


if __name__ == '__main__':
    dados = lendo_arquivo_entrada()

    total, media, dados = calculo(dados)

    dados = ordenar_dados(dados)

    criar_relatorio()

    adicionando_dados_relatorio(dados, total, media, int(input("Quantidade de pessoas a serem mostrados no arquivo: ")))
