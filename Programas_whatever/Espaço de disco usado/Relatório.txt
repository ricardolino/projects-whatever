ACME Inc.               Uso do espaço em disco pelos usuários
------------------------------------------------------------
Nr.  Usuário        Espaço utilizado     % de uso

1    anderson       1187.99                 46.02
2    rosemary       752.88                  29.16
3    alexandre      434.99                  16.85
4    antonio        117.74                   4.56
5    carlos         87.03                    3.37
6    cesar          0.94                     0.04

Espaço total ocupado: 2581.57 MB
Espaço médio ocupado: 430.26 MB