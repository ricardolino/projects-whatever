import random

num_repeticoes = int(input("Número de repetições: "))
num_pessoas = int(input("Número de datas: "))
print()

casos_favoráveis = 0
for i in range(num_repeticoes):
    l = [str(random.randint(1, 31)) + '/' + str(random.randint(1,12)) + '/' + str(random.randint(2003,2005)) for i in range(num_pessoas)]

    for data in l:
        if l.count(data) > 1:
            casos_favoráveis += 1
            break

print(f"Probabilide do acontecimento: {casos_favoráveis/num_repeticoes*100}%")