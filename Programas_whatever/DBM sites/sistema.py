"""
Crie um database de sites permitindo que o usuário
acesse esse database recupere a descriçao de cada site
e tambem coloque novos sites. Tenha certeza que as entradas do
usuário para sites sejam válidas, www.nomedosite.com.br, www.python.org
"""


def validação_site():
    import urllib.request

    while True:
        site = input("Site (www.nomedosite.com.br): ")
        site_aux = 'http://' + site

        try:
            urllib.request.urlopen(site_aux)
        except:
            print('ERRO! Siga o padrão www.nomedosite.com.br')
        else:
            return site


def criar_banco_dados():
    import dbm

    banco = dbm.open('Sites.db', 'c')

    continuar = 'S'
    while continuar.startswith('S'):
        site = validação_site().encode()
        titulo_site = input("Titulo ou nome do site: ").encode()
        banco[site] = titulo_site

        continuar = ''
        while continuar != 'S' and continuar != 'N':
            continuar = input("Deseja continuar ? [Sim/Não]: ").upper()

    banco.close()

    return banco


if __name__ == '__main__':
    banco = criar_banco_dados()

    for i in banco.keys():
        print(f"Nome Site: {i.decode()} Site: {banco[i].decode()}")
