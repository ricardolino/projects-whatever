"""
Escreva um programa para checar se uma senha numérica está correta

Pergunte para o usuário se ele quer definir uma senha,
veficar o usuário ou sair.

Escreva, com apenas uma linha, uma forma de
verificar se a senha está correta
"""


def definir_senha():
    global senha
    senha = input("Senha: ")
    print()


if __name__ == '__main__':
    senha = ''

    continuar = True
    while continuar:

        decisao_usuario = 0
        while 1 > decisao_usuario or 3 < decisao_usuario:
            decisao_usuario = int(input("1 - Definir senha\n2 - Verificar Senha\n3 - Sair"
                                        "\nDigite uma opção: "))
            print()

        if decisao_usuario == 1:
            definir_senha()
        elif decisao_usuario == 2:
            print("Acesso liberado" if all(map((lambda tentativa:senha==tentativa), [input("Senha: ")])) else "Acesso Negado")
            print()
        else:
            continuar = False

