from tkinter import *

class App4Canvas(object):
	def __init__(self, i_principal):
		self.root = i_principal
		self.frameCanvas = Frame(self.root)
		self.frameCanvas.pack()
		self.CanvasCarinha = Canvas(self.frameCanvas, width=250, height=250, bg='deepskyblue')
		self.CanvasCarinha.pack()
		#Carinha
		self.CanvasCarinha.create_oval(110, 110, 140, 140, fill='yellow', outline='black', tag='carinha')
		self.CanvasCarinha.create_oval(117,117,121,121, fill='black', tag='carinha')
		self.CanvasCarinha.create_oval(128,117, 132, 121, fill='black', tag='carinha')
		self.CanvasCarinha.create_arc(110, 107, 140, 135, start=220, extent=100, style=ARC, tag='carinha')
		#Botão start
		self.botãoStartCarinha = Button(self.root, text='START', command=self.iniciarMovimentação)
		self.botãoStartCarinha.pack()
		#Variáveis de movimentação
		self.movimentação_x = 5
		self.movimentação_y = 4
		self.eixo_x = self.eixo_y = 125

	def iniciarMovimentação(self):
		self.CanvasCarinha.move('carinha', self.movimentação_x, self.movimentação_y)
		self.eixo_x += self.movimentação_x
		self.eixo_y += self.movimentação_y

		if self.eixo_x<=0 or self.eixo_x>=250:
			self.movimentação_x *= -1

		if self.eixo_y<=0 or self.eixo_y>=250:
			self.movimentação_y *= -1

		self.root.after(25, self.iniciarMovimentação)