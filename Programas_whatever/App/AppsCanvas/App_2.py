from tkinter import *

class App2Canvas(object):
	def __init__(self, i_principal):
		self.frameCanvas = Frame(i_principal)
		self.frameCanvas.pack()
		self.CanvasLogo = Canvas(self.frameCanvas, bg='blue', height=150, width=150)
		self.CanvasLogo.pack()
		self.CanvasLogo.create_polygon((20, 50), (75, 130), (130, 50), (130, 20), (20, 20), fill='white')
		self.CanvasLogo.create_rectangle((23, 23), (127, 47), fill='black')
		self.CanvasLogo.create_polygon((30, 55), (73, 120), (73, 55),fill='red')
		self.CanvasLogo.create_polygon((77, 120), (77, 55), (120, 55), fill='black')
		self.CanvasLogo.create_text(75, 35, text='SPFC', fill='white', font=('Arial', 20, 'bold'))
