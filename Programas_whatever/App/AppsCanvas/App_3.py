from tkinter import *

class App3Canvas(object):
	def __init__(self, i_principal):
		self.frameCanvas = Frame(i_principal)
		self.frameCanvas.pack()
		self.CanvasFatia = Canvas(self.frameCanvas, height=200, width=200, bg='black')
		self.CanvasFatia.pack()
		self.frameInteraçãoCanvasFatia = Frame(i_principal)
		self.frameInteraçãoCanvasFatia.pack()
		self.LabelFatia = Label(self.frameInteraçãoCanvasFatia, text='Fatia:', fg='blue')
		self.LabelFatia.pack(side=LEFT)
		self.Entry = Entry(self.frameInteraçãoCanvasFatia)
		self.Entry.pack(side=LEFT)
		self.Labelporcentagem = Label(self.frameInteraçãoCanvasFatia, text='%', fg='blue')
		self.Labelporcentagem.pack(side=LEFT)
		self.botãoDesenhar = Button(self.frameInteraçãoCanvasFatia, text='Desenhar', fg='blue', command=self.arco)
		self.botãoDesenhar.pack(side=LEFT)
		self.CanvasFatia.create_oval(1, 1, 200, 200, fill='blue')

	def arco(self):
		if self.Entry.get() == '':
			raio = 90
		else:
			raio = (360 * float(self.Entry.get())) / 100
			self.CanvasFatia.create_arc(1, 1, 200, 200, fill='yellow', extent=raio)
		
		