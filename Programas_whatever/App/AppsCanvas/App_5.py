from tkinter import *

class App5Canvas(object):
	def __init__(self, i_principal):
		self.frameCanvas = Frame(i_principal)
		self.frameCanvas.pack()
		self.CanvasCarinhaSeta = Canvas(self.frameCanvas, width=250, height=250, bg='deepskyblue')
		self.CanvasCarinhaSeta.pack()
		#Carinha
		self.CanvasCarinhaSeta.create_oval(110, 110, 140, 140, fill='yellow', outline='black', tag='carinha_seta')
		self.CanvasCarinhaSeta.create_oval(117,117,121,121, fill='black', tag='carinha_seta')
		self.CanvasCarinhaSeta.create_oval(128,117, 132, 121, fill='black', tag='carinha_seta')
		self.CanvasCarinhaSeta.create_arc(110, 107, 140, 135, start=220, extent=100, style=ARC, tag='carinha_seta')
		#Movimentação
		self.CanvasCarinhaSeta.focus_force()
		self.CanvasCarinhaSeta.bind('<Up>', self.pracimaCarinha)
		self.CanvasCarinhaSeta.bind('<Down>', self.prabaixoCarinha)
		self.CanvasCarinhaSeta.bind('<Right>', self.direitaCarinha)
		self.CanvasCarinhaSeta.bind('<Left>', self.esquerdaCarinha)

	def pracimaCarinha(self, evento):self.CanvasCarinhaSeta.move('carinha_seta', 0, -5)

	def prabaixoCarinha(self, evento):self.CanvasCarinhaSeta.move('carinha_seta', 0, 5)

	def direitaCarinha(self, evento):self.CanvasCarinhaSeta.move('carinha_seta', 5, 0)

	def esquerdaCarinha(self, evento):self.CanvasCarinhaSeta.move('carinha_seta', -5, 0)
