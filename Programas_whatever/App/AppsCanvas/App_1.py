from tkinter import *
class App1Canvas(object):
	def __init__(self, i_principal):
		self.frameCanvas = Frame(i_principal)
		self.frameCanvas.pack()
		self.CanvasJogo = Canvas(self.frameCanvas, bg='black', height=350)
		self.CanvasJogo.pack()
		self.frameBotõesJogo = Frame(i_principal)
		self.frameBotõesJogo.pack()
		self.eixox = 0
		self.eixoy = 160
		self.botãoEsquerda = Button(self.frameBotõesJogo, text='Esquerda', fg='blue', command=self.esquerda)
		self.botãoEsquerda.pack(side=LEFT)	
		self.botãoParaCima = Button(self.frameBotõesJogo, text='Para Cima', fg='blue', command=self.cima)
		self.botãoParaCima.pack(side=LEFT)
		self.botãoParaBaixo = Button(self.frameBotõesJogo, text='Para Baixo', fg='blue', command=self.baixo)
		self.botãoParaBaixo.pack(side=LEFT)
		self.botãoDireita = Button(self.frameBotõesJogo, text='Direita', fg='blue', command=self.direita)
		self.botãoDireita.pack(side=LEFT)

	def esquerda(self):
		self.CanvasJogo.create_line(self.eixox, self.eixoy, self.eixox-40, self.eixoy, fill='green')
		self.eixox -= 40

	def cima(self):
		self.CanvasJogo.create_line(self.eixox, self.eixoy, self.eixox, self.eixoy-40, fill='yellow')
		self.eixoy -= 40

	def baixo(self):
		self.CanvasJogo.create_line(self.eixox, self.eixoy, self.eixox, self.eixoy+40, fill='blue')
		self.eixoy += 40

	def direita(self):
		self.CanvasJogo.create_line(self.eixox, self.eixoy, self.eixox+40, self.eixoy, fill='purple')
		self.eixox += 40