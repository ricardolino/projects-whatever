from tkinter import *

class App6Canvas(object):
	def __init__(self, i_principal):
		self.frameCanvas = Frame(i_principal)
		self.frameCanvas.pack()
		self.CanvasMario = Canvas(self.frameCanvas, width=250, height=125, bg='black')
		self.CanvasMario.pack()
		#Adiconando imagens Mario a lista
		self.imagensMario = []
		for i in range(1,5):
			self.imagensMario.append(PhotoImage(file=f'Imagens/imagens-mario/mario_{i}.ppm'))
		for i in range(1,5):
			self.imagensMario.append(PhotoImage(file=f'Imagens/imagens-mario/mario_l{i}.ppm'))
		#Botão Start
		self.botãoStartMario = Button(self.frameCanvas, text='Start', command=self.inicioMovimentaçãoMario)
		self.botãoStartMario.pack()

	def inicioMovimentaçãoMario(self):
		self.CanvasMario.create_image((125, 100), image=self.imagensMario[3], tag='MarioMexendo')
		self.botãoStartMario['command'] = self.movimentaçãoMario

	def movimentaçãoMario(self):
		self.CanvasMario.focus_force()
		self.incrementoMovimentaçãoMario = 1
		self.CanvasMario.bind('<Right>', self.direitaMario)
		self.CanvasMario.bind('<Left>', self.esquerdaMario)

	def direitaMario(self, evento):
		self.CanvasMario.itemconfig('MarioMexendo', image=self.imagensMario[self.incrementoMovimentaçãoMario-1])
		self.incrementoMovimentaçãoMario += 1 
		if self.incrementoMovimentaçãoMario == 5:
			self.incrementoMovimentaçãoMario = 1
		self.CanvasMario.move('MarioMexendo', 15, 0)

	def esquerdaMario(self, evento):
		self.CanvasMario.itemconfig('MarioMexendo', image=self.imagensMario[-self.incrementoMovimentaçãoMario])
		self.incrementoMovimentaçãoMario += 1 
		if self.incrementoMovimentaçãoMario == 5:
			self.incrementoMovimentaçãoMario = 1
		self.CanvasMario.move('MarioMexendo', -15, 0)
