#!/usr/bin/env python3
"""
Criar interface com login e senha um botão de acesso
De acordo com login.db escrever:
Usuário inválido
Senha Inválida
Usuário e senha inválido
Acesso Permitido
"""
from tkinter import *
from time import sleep
import sys
sys.path.append('AppsCanvas')
from App_1 import App1Canvas
from App_2 import App2Canvas
from App_3 import App3Canvas
from App_4 import App4Canvas
from App_5 import App5Canvas
from App_6 import App6Canvas
import shelve

class CriandoInterface(object):
    def __init__(self, i_principal):
        i_principal.resizable(False, False)
        #Logo
        self.frameLogo = Frame(i_principal)
        self.frameLogo.pack()
        self.logo = Label(self.frameLogo)
        self.instanciaLogo = PhotoImage(file='Imagens/bg_python.gif')
        self.logo['image'] = self.instanciaLogo
        self.logo.pack()
        #Interação
        self.frameInteração = Frame(i_principal)
        self.frameInteração.pack()
        self.tituloUsuario = Label(self.frameInteração, text='Usuário')
        self.tituloUsuario.pack()
        self.formularioUsuario = Entry(self.frameInteração)
        self.formularioUsuario.pack()
        self.tituloSenha = Label(self.frameInteração, text='Senha')
        self.tituloSenha.pack()
        self.formularioSenha = Entry(self.frameInteração, show='*')
        self.formularioSenha.pack()
        #Lembrando
        file = shelve.open('login.db')
        if 'lembrete' in file:
            self.formularioUsuario.insert(END, file['lembrete'][0])
            self.formularioSenha.insert(END, file['lembrete'][1])
        file.close()
        #Lembra me
        self.frameLembrete = Frame(i_principal)
        self.frameLembrete.pack()
        self.lembra_me = False
        self.checkLembrete = Checkbutton(self.frameLembrete, text='Lembra-me', command=self.lembrete)
        self.checkLembrete.pack()
        #Botões
        self.frameBotões = Frame(i_principal)
        self.frameBotões.pack()
        self.botão = Button(self.frameBotões, command=self.verificaValidade)
        self.instanciaBotãoEntrar = PhotoImage(file='Imagens/b_entrar.ppm')
        self.botão['image'] = self.instanciaBotãoEntrar
        self.botão.pack(side=LEFT)
        self.botãoNovoUsuario = Button(self.frameBotões, command=self.NovoUsuário)
        self.instanciaBotãoNovoUsuario = PhotoImage(file='Imagens/b_novo.ppm')
        self.botãoNovoUsuario['image'] = self.instanciaBotãoNovoUsuario
        self.botãoNovoUsuario.pack(side=RIGHT)
        #Criando acesso (email e nome), precisa está em baixo do franme de interação, por causa da variável self.frameBotões
        self.frameCriandoAcesso = Frame(i_principal)
        self.tituloNome = Label(self.frameCriandoAcesso, text='Nome')
        self.tituloNome.pack()
        self.formularioNome = Entry(self.frameCriandoAcesso)
        self.formularioNome.pack()
        self.tituloEmail = Label(self.frameCriandoAcesso, text='Email')
        self.tituloEmail.pack()
        self.formularioEmial = Entry(self.frameCriandoAcesso)
        self.formularioEmial.pack()
        self.botãoCriar = Button(self.frameBotões, command=self.CriarUsuario)
        self.instaciaBotãoCriar = PhotoImage(file='Imagens/b_criar.ppm')
        self.botãoCriar['image'] = self.instaciaBotãoCriar
        #Responsta
        self.frameResposta = Frame(i_principal)
        self.frameResposta.pack()
        self.resposta = Label(self.frameResposta)
        self.resposta.pack()

    def verificaValidade(self):
        #file[usuário] = [senha, email, nome]
        file = shelve.open('login.db')
        if self.formularioSenha.get().strip() == '' or self.formularioUsuario.get().strip() == '':
            self.resposta['text'] = 'Preencha todos os campos'
        else:
            if self.formularioUsuario.get().strip() in file:
                if file[self.formularioUsuario.get().strip()][0] == self.formularioSenha.get().strip():
                    self.BotãoEntrarapp()
                else:
                    self.resposta['text'] = 'Senha inválida'
                    self.resposta['fg'] = 'red'
            else:
                self.resposta['text'] = 'Usuário ou senha inválido'
                self.resposta['fg'] = 'red'
        file.close()

    def NovoUsuário(self):
        #Esquecendo empacotamentos
        self.frameLogo.pack_forget()
        self.frameLembrete.pack_forget()
        self.botão.pack_forget()
        self.botãoNovoUsuario.pack_forget()
        self.frameInteração.pack_forget()
        self.frameResposta.pack_forget()
        self.frameBotões.pack_forget()    
        #Empacotando novas interações
        self.frameCriandoAcesso.pack()
        self.frameInteração.pack()
        self.frameBotões.pack()
        self.botãoCriar.pack()
        self.frameResposta.pack()

    def CriarUsuario(self):
        #file[usuário] = [senha, email, nome]
        file = shelve.open('login.db')
        if self.formularioNome.get().strip()=='' or self.formularioEmial.get().strip()=='' or self.formularioUsuario.get().strip()=='' or self.formularioSenha.get().strip()=='':
            self.resposta['text'] = 'Preencha todos os campos'
        else:
            if self.formularioUsuario.get().strip() in file:
                self.resposta['text'] = 'Usuário já existente'
                self.resposta['fg'] = 'black'
            else:
                file[self.formularioUsuario.get().strip()] = [self.formularioSenha.get().strip(), self.formularioEmial.get().strip(), self.formularioNome.get().strip()]
                self.formularioNome.delete(0, END)
                self.formularioEmial.delete(0, END)
                self.reoganizar()

    def reoganizar(self):
        self.botãoCriar.pack_forget()
        self.frameInteração.pack_forget()
        self.frameBotões.pack_forget()
        self.frameResposta.pack_forget()
        self.frameCriandoAcesso.pack_forget()
        self.frameLogo.pack()
        self.frameInteração.pack()
        self.frameLembrete.pack()
        self.frameBotões.pack()
        self.botão.pack(side = LEFT)
        self.botãoNovoUsuario.pack(side = RIGHT)
        self.frameResposta.pack()
        self.resposta['text'] = 'Usuário criado com sucesso'
        self.resposta['fg'] = 'green'

    def lembrete(self):
        self.lembra_me = not self.lembra_me
        file = shelve.open('login.db')
        if self.lembra_me and (self.formularioUsuario.get().strip() in file):
            if file[self.formularioUsuario.get().strip()][0] == self.formularioSenha.get().strip():
                file['lembrete'] = [self.formularioUsuario.get().strip(), self.formularioSenha.get().strip()]
        file.close()

    def BotãoEntrarapp(self):
        #Destruindo interface
        self.frameLogo.destroy()
        self.frameInteração.destroy()
        self.frameLembrete.destroy()
        self.frameBotões.destroy()
        self.frameResposta.destroy()
        #Iniciando nova interface
        self.frameBotãoIniciarJogo = Frame(i_principal)
        self.frameBotãoIniciarJogo.pack()
        self.botãoJogoLinha = Button(self.frameBotãoIniciarJogo, text='App Canvas 1 - Linhas', command=self.JogoLinha)
        self.botãoJogoLinha.pack()
        self.botãoSPFC = Button(self.frameBotãoIniciarJogo, text='App Canvas 2 - SPFC', command=self.logoSPFC)
        self.botãoSPFC.pack()
        self.botãoFatia = Button(self.frameBotãoIniciarJogo, text='App Canvas 3 - Fatias', command=self.fatia)
        self.botãoFatia.pack()
        self.botãoCarinha = Button(self.frameBotãoIniciarJogo, text='App Canvas 4 - Carinha', command=self.carinha)
        self.botãoCarinha.pack()
        self.botãoCarinhaSeta = Button(self.frameBotãoIniciarJogo, text='App Canvas 5 - Carinha Seta', command=self.carinhaSeta)
        self.botãoCarinhaSeta.pack()
        self.botãoMario = Button(self.frameBotãoIniciarJogo, text='App Canvas 6 - Mario', command=self.mario)
        self.botãoMario.pack()

    def JogoLinha(self):
        #Destruindo interface
        self.frameBotãoIniciarJogo.destroy()
        #Iniciando nova interface
        App1Canvas(i_principal)

    def logoSPFC(self):
        #Destruindo interface
        self.frameBotãoIniciarJogo.destroy()
        #Iniciando nova interface
        App2Canvas(i_principal)

    def fatia(self):
        #Destruindo interface
        self.frameBotãoIniciarJogo.destroy()
        #Iniciando nova interface
        App3Canvas(i_principal)

    def carinha(self):
        #Destruinfo interface anterior
        self.frameBotãoIniciarJogo.destroy()
        #Iniciando nova Interface
        App4Canvas(i_principal)

    def carinhaSeta(self):
        #Destruindo frame botões
        self.frameBotãoIniciarJogo.destroy()
        #Iniciando interface carinha seta
        App5Canvas(i_principal)

    def mario(self):
        #Destruindo frame botões
        self.frameBotãoIniciarJogo.destroy()
        #Iniciando nova interface
        App6Canvas(i_principal)


if __name__ == '__main__':
    i_principal = Tk()

    i_principal.title('App Jogos')

    CriandoInterface(i_principal)

    i_principal.mainloop()
