import threading

class Ler(threading.Thread):
    def __init__(self, arq):
        threading.Thread.__init__(self)
        self.lock = threading.Lock()
        self.total = 0
        self.arq = arq

    def run(self):
        for i in range(1000):
            with self.lock:
                self.total += int(self.arq.readline())
