#Construa uma lista com os nomes do clientes para organizar em uma barbearia a ordem de quem ira cortar o cabelo primero,
#na lista deve conter o nome do usuario e a sua posição, Além disso deve conter os elementos para criar, alterar, ver e apagar

def menu():
    escolha = ['c', 'r', 'u', 'd', 's']
    opcao = ''

    while opcao not in escolha:
        print(40 * '-=')
        print("                      BEM - VINDO A BARBEARIA")
        print('MENU:')
        print('OBS: ESCOLHA ALGUMA DESSAS LETRAS ABAIXO PARA  MEXER NA LISTA. \n')
        print('[c] - ADCIONAR NOME')
        print('[r] - VER A LISTA ')
        print('[u] - ATUALIZAR')
        print('[d] - DELETAR')
        print('[s] - SAIR')
        print()

        opcao = input('QUAL É A SUA OPÇÃO? ').lower()            #Digite a opção na qual você deseja

        if opcao not in escolha:
            print('OPÇÃO INVALIDA, TENTE NOVAMENTE.')
    return opcao


def created():                           # Def para a criação de uma lista
    pessoa.append(input('CLIENTE: ').title())
    pessoa.append(input('CPF: '))
    lista.append(pessoa[:])
    pessoa.clear()
    import time
    time.sleep(1)
    print('NOME ADCIONADO NA LISTA')


def read():                                            #Def para ler a lista ou um nome especifico
        if len(lista) == 0:
            print('LISTA VAZIA')

        else:
            print('POSIÇÃO -- CLIENTES(CPF)')
            for id, cad in enumerate(lista):
                print(f'{id+1}º -- {cad[0]}({cad[1]})')
            import time
            time.sleep(1)


def atualizando():
    alteração = int(input("1 - Alterar nome ou CPF"
                        "\n2 - Adicionar em local específico"
                        "\nOpção: "))
    if alteração == 1:
        alt = int(input("Alterar dados de qual POSIÇÃO: ")) - 1
        lista[alt][0] = input("NOME CORRETO: ")
        lista[alt][1] = input("CPF CORRETO: ")
        print('MODIFICAÇÃO CONFIRMADA')
    elif alteração == 2:
        pos = int(input('Digite a POSIÇÃO que vai adicionar o CLIENTE: ')) - 1
        pessoa.append(input('CLIENTE: ').title())
        pessoa.append(input('CPF: '))
        lista.insert(pos, pessoa[:])
        pessoa.clear()
        print('MODIFICAÇÃO CONFIRMADA')


def deleted():                                                #Apagar algo da lista
    removendo = int(input('Removendo o CLIENTE da POSIÇÃO? ')) - 1
    if len(lista) == 0:
        print('LISTA VAZIA!')
    elif removendo > len(lista)-1:
        print(f'Não tem está posição')
    else:
        lista.pop(removendo)
        print('MODIFICAÇÃO CONFIRMADA')
        import time
        time.sleep(1)


if __name__ == '__main__':
    lista = []
    pessoa = []
    while True:
        retorno = menu()
        if retorno == 'c':
            created()
        elif retorno == 'r':
            read()
        elif retorno == 'u':
            atualizando()
        elif retorno == 'd':
            deleted()
        elif retorno == 's':
            print(40 * '-=')
            print('                                 FIM DO PROGRAMA')
            break
        else:
            print("ERRO!")