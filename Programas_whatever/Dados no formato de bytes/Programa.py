"""
Crie um programa para o governo, onde é possível armazenar o nome
de uma Pessoa, R.G. e CPF
dados num formato de bytes
3.313.452
123.456.789-10
"""


def leiaInt(msg):
    """
    :param msg: Recebe uma mensagem que será mostrada
    para o usuário nomoneto em que o mesmo for inserir o dado.
    :return: Returna o mesmo dado
    Em caso do número não dor um número inteiro irá mostrar uma mensagem de erro
    """

    while True:
        try:
            num = input(msg).strip()
            num = int(num)
        except (ValueError, TypeError):
            print("ERRO! Digite 1 ou 2")
            continue
        except KeyboardInterrupt:
            print()
            return 0
        else:
            return int(num)


def valida_rg(msg):
    """
    :param msg: Recebe uma mensagem que será mostrada
    para o usuário nomoneto em que o mesmo for inserir o dado.
    :return: Retorna o rg validado
    Em caso do rg estiver fora do padrão (n.nnn.nnn) irá mostra uma mensagem de erro
    """

    while True:
        rg = input(msg).strip()

        if len(rg) == 9:
            if 2 == rg.count('.') and rg[1] == '.' and rg[5] == '.':
                if rg.split('.')[0].isnumeric() and rg.split('.')[1].isnumeric() and \
                        rg.split('.')[2].isnumeric():
                    return rg
                else:
                    print("ERRO! Tem caracter (letra, sinal, ponto etc) não com patível com rg (n.nnn.nnn)")
                    print()
            else:
                print("ERRO! Pontos (.) do rg mal posicionados (n.nnn.nnn)")
                print()
        else:
            print("ERRO! Tamanho não compativel o padrão do rg (n.nnn.nnn)")
            print()


def validar_cpf(msg):
    """
    :param msg: Recebe uma mensagem que será mostrada
    para o usuário nomoneto em que o mesmo for inserir o dado.
    :return: Retorna o cpf validado
    Em caso do cpf estiver fora do padrão (nnn.nnn.nnn-nn) irá mostra uma mensagem de erro
    """

    while True:
        cpf = input(msg).strip()

        if len(cpf) == 14:
            if 2 == cpf.count('.') and 1 == cpf.count('-'):
                if cpf[3] == '.' and cpf[7] == '.' and cpf[11] == '-':
                    if cpf.split('.')[0].isnumeric() and cpf.split('.')[1].isnumeric() and \
                            (cpf[8] + cpf[9] + cpf[10]).isnumeric() and (cpf[12] + cpf[13]).isnumeric():
                        return cpf
                    else:
                        print(
                            "ERRO! Tem caracter (letra, sinal, ponto etc) não com patível com o padrão (nnn.nnn.nnn-nn)")
                        print()
                else:
                    print("ERRO! Pontos ou traço mal posicionados (nnn.nnn.nnn-nn)")
                    print()
            else:
                print("ERRO! Quantidade de pontos (.) ou traço (-) fora do padrão (nnn.nnn.nnn-nn)")
        else:
            print("ERRO! Tamanho não compativel o padrão (nnn.nnn.nnn-nn)")


def recebe_dados():
    """
    :return: Retona o nome, RG, CPF da pessoa em string
    """

    pessoas = []
    pessoa = []

    while True:
        nome = input("Nome: ").strip().title().encode()
        pessoa.append(nome)

        rg = valida_rg("RG: ")
        pessoa.append(rg)

        cpf = validar_cpf("CPF: ")
        pessoa.append(cpf)

        pessoas.append(pessoa.copy())
        pessoa.clear()
        print()

        continuar = 0
        while continuar != 1 and continuar != 2:
            continuar = leiaInt("Deseja continuar cadastrando pessoas ? \n[1 - Sim / 2 - Não]: ")
            print()

        if continuar == 2:
            break

    return pessoas


def tranforma_int(pessoas):
    """
    :param pessoas: Listas com as pessoas cadastradas
    :return: Retorna a lista com todas as posições [n][1] e [n][2] no formato inteiro
    """

    for i in pessoas:
        rg_string = i[1].split('.')

        rg = (int(rg_string[0]) * 1000000) + (int(rg_string[1][0]) * 100000) + (int(rg_string[1][1]) * 10000) + \
             (int(rg_string[1][2]) * 1000) + (int(rg_string[2][0]) * 100) + (int(rg_string[2][1]) * 10) + (
                 int(rg_string[2][2]))

        i[1] = rg

        cpf_string = i[2].split('.')
        partefinal_cpf = cpf_string[2].split('-')
        cpf_string[2] = partefinal_cpf[0]
        cpf_string.append(partefinal_cpf[1])

        cpf = (int(cpf_string[0][0]) * 10000000000) + (int(cpf_string[0][1]) * 1000000000) + (
                    int(cpf_string[0][2]) * 100000000) + \
              (int(cpf_string[1][0]) * 10000000) + (int(cpf_string[1][1]) * 1000000) + (
                          int(cpf_string[1][2]) * 100000) + \
              (int(cpf_string[2][0]) * 10000) + (int(cpf_string[2][1]) * 1000) + (int(cpf_string[2][2]) * 100) + \
              (int(cpf_string[3][0]) * 10) + (int(cpf_string[3][1]))

        i[2] = cpf

    return pessoas


def codifica(pessoas):
    """
    :param pessoas: Listas com as pessoas cadastradas
    :return: Retorna um lista com todos os dados de uma pessoa codificada
    """
    import struct

    pessoas_cod = []

    for i in pessoas:
        tam = len(i[0])
        formato = f'{tam}s Q Q'
        pessoas_cod.append(struct.pack(formato, *i))
    return pessoas_cod


def escreve_arquivo(pessoas_cod):
    """
    :param pessoas_cod: Listas com as pessoas cadastradas e dados codificados
    :return: null
    Escreve o arquivo com a lista codificada
    """

    try:
        arq = open('arquivo.db', 'wb')
    except:
        print("ERRO ao criar arquivo")
        return

    for i in pessoas_cod:
        arq.write(i)
        arq.write(b'\n')

    return


if __name__ == '__main__':
    pessoas = recebe_dados()

    tranforma_int(pessoas)

    pessoas_cod = codifica(pessoas)

    escreve_arquivo(pessoas_cod)
