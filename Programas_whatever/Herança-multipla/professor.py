from pessoa import *
class Professor(Pessoa):
	def __init__(self, nome, idProfessor):
		Pessoa.__init__(self, nome)
		self.idProfessor = idProfessor

	def __str__(self):
		return f'Nome: {self.nome} / Código: {self.idProfessor}'