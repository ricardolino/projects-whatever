from pessoa import *
class Aluno(Pessoa):
	def __init__(self, nome, idAluno):
		Pessoa.__init__(self, nome)
		self.idAluno = idAluno

	def __str__(self):
		return f'Nome: {self.nome} / Código: {self.idAluno}'