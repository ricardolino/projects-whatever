from aluno import *
from professor import *

class ProfessorAluno(Aluno, Professor):
	def __init__(self, nome, idAluno, idProfessor, idAlunoProfessor):
		Aluno.__init__(self, nome, idAluno)
		Professor.__init__(self, nome, idProfessor)
		self.idAlunoProfessor = idAlunoProfessor

	def __str__(self):
		return f'Nome: {self.nome} / Código Aluno: {self.idAluno} / Código Professor: {self.idProfessor} / Código Aluno-Profesor: {self.idAlunoProfessor}'