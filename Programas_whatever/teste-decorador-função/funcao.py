def p_decorador(func):
    def nova_func(num1, num2):
        return f'<p>{func(num1, num2)}</p>'
    return nova_func


def recebe_parametro_decorador(parametro):
    def p_decorador2(func):
        def nova_func(num1, num2):
            return f'<{parametro}>{func(num1, num2)}<{parametro}>'
        return nova_func
    return p_decorador2


@recebe_parametro_decorador('strong')
def trecho(num1, num2):
    return f'Resultado: {num1*num2}'


print(trecho(2, 3))
