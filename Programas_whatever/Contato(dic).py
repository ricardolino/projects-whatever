"""
Escreva um programa que receba quantas entradas o usuário desejar e depois
crie um novo contato para cada entrada (Nome, Telefone, Endereço, Email), e
por fim imprima, em ordem alfabética, a agenda de contatos 
"""
def devolve_posicao_listaAtualizada(nome, lista):
    lista.append(nome)
    lista.sort()
    return lista.index(nome), lista

listaFinal = []
lista = []

continua = 1
while continua == 1:
    nome = input("Nome: ").strip().title()

    if len(lista) == 0:
        lista.append(nome)
        posicao = 0
    else:
        posicao, lista = devolve_posicao_listaAtualizada(nome, lista)
    
    listaFinal.insert(posicao, {'Nome':nome, 'Telefone':int(input("Telefone: ")),
                  'Endereço':input("Endereço: ").strip().title(), 'Email':input("Email: ").strip()})

    print()

    continua = 3
    while continua != 1 and continua != 2:
        continua = int(input("1 Para continua / 2 - Para interromper: "))

    print()

for i in listaFinal:
    for j in i:
        print(f"{j}: {i[j]}")
    print()
