def linha(lista):
    if (lista[0] + lista[1] + lista[2]) == (lista[3] + lista[4] + lista[5]) == (lista[6] + lista[7] + lista[8]):
        return lista[0] + lista[1] + lista[2]
    else:
        return -1


def coluna(lista):
    if (lista[0] + lista[3] + lista[6]) == (lista[1] + lista[4] + lista[7]) == (lista[2] + lista[5] + lista[8]):
        return lista[0] + lista[3] + lista[6]
    else:
        return -2


def diagonal(lista):
    if (lista[0] + lista[4] + lista[8]) == (lista[2] + lista[4] + lista[6]):
        return lista[0] + lista[4] + lista[8]
    else:
        return -3


def forma_quadrado(lista):
    if linha(lista) == coluna(lista) == diagonal(lista):
        return True
    else:
        return False


def constriundo_quadrado():
    import random

    lista = list(range(1, 10))

    ct = 0
    while True:
        random.shuffle(lista)

        print("Procurando quadrado mágico")
        print()

        if forma_quadrado(lista):
            print("Encontrei !")
            print()
            ct += 1
            for i in range(1, 10):
                print(lista[i-1], end=' ')
                if i % 3 == 0:
                    print()

        print()

        if ct == 2:
            break


constriundo_quadrado()
