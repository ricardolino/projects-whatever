candidatos = []
votos = []

print("Escolha o melhor da partida, usando o número da camisa")
print("Só jogaram jodares com a camisa de 1 a 22")

while True:
    while True:
        v = int(input("Camisa do melhor jodaor da partida: "))
        if 0 <= v <= 22:
            break
        else:
            print("ERRO, digite um número de 1 a 22")
    if v == 0:
        break

    if v not in candidatos:
        candidatos.append(v)

    votos.append(v)

print(f"Quantidades de votos {len(votos)}")

for pos, i in enumerate(candidatos):
    print(f"Camisa {i}: {100*votos.count(i)/len(votos):.1f}%")