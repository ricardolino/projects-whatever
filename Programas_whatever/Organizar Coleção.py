# Organiza em uma tabela livros, hqs, filmes e séries de um colecionador
# Deve se ter em uma tabela todos os nomes, criador, quantidade e tipo de "exemplar"

import sqlite3
from sqlite3 import Error


def inserir():
    comando_sql = 'insert into tb_amostras(nome, tipo_exemplar, criador, quantidade) values(?,?,?,?)'

    print("O campo que não souber coloque desconhacido")
    print()
    tipo_exempla1 = input("Tipo de exempla: ").title().strip()
    nome1 = input(f"Nome do {tipo_exempla1}: ").title().strip()
    criador1 = input(f"Nome do criador/diretor/autor do {tipo_exempla1}: ").title().strip()
    quantidade1 = int(input("Quantidade de exemplares: "))

    try:
        cur.execute(comando_sql, (nome1, tipo_exempla1, criador1, quantidade1))
        conexao.commit()
    except Error as e:
        print(f"Erro no comando sql")
        print(e)
        conexao.rollback()
    except:
        print("Erro ao inserir dado")
    else:
        print()
        print(f"{nome1} adicionado com sucesso")

    return


def selecionar():
    print("O que você quer ver na tabela ?")
    opcao_selecao = ''
    while opcao_selecao != 1 and opcao_selecao != 2:
        opcao_selecao = int(input(f"1 - Ver tudo \n2 - Ver arquivos específicos \nOpção: "))

    if opcao_selecao == 1:
        comando_sql = 'select * from tb_amostras'

        try:
            cur.execute(comando_sql)

            lista_registros = cur.fetchall()
        except Error as e:
            print("Erro no comando sql")
            print(e)
            conexao.rollback()
        except:
            print("Erro ao selecionar arquivos")
        else:
            print()
            for r in lista_registros:
                print(f"ID: {r[0]} | Nome: {r[1]} | Tipo de exemplar: {r[2]} | Criador/Diretor/Autor: {r[3]} | Quantidade: {r[4]} ")
    else:
        dado_retrincao = input("Restringor seleção a linha que estiver com o dado: ").title().strip()
        coluna_restricao = input(f"O dado '{dado_retrincao}' faz parte de qual coluna? ").lower().strip()

        if coluna_restricao == "id":
            coluna_restricao = "pk_id"
        elif coluna_restricao == "tipo de exemplar":
            coluna_restricao = "tipo_exemplar"
        elif coluna_restricao == "diretor" or coluna_restricao == "autor":
            coluna_restricao = "criador"
        elif coluna_restricao == 'quantidade':
             try:
                 dado_retrincao = int(dado_retrincao)
             except:
                 print("Erro ao transforma em inteiro")

        comando_sql = f"select * from tb_amostras where {coluna_restricao}=?"

        try:
            cur.execute(comando_sql, (dado_retrincao, ))

            lista_resgistros = cur.fetchall()
        except Error as e:
            print("Erro no comando sql")
            print(e)
            conexao.rollback()
        except:
            print("Erro a seleinar Tabela")
        else:
            print()
            for r in lista_resgistros:
                print(f"ID: {r[0]} | Nome: {r[1]} | Tipo de exemplar: {r[2]} | Criador/Diretor/Autor: {r[3]} | Quantidade: {r[4]} ")

        return


def atualizar():
    dado_retrincao = input("Restringor seleção a linha que estiver com o dado: ").title().strip()
    coluna_restricao = input(f"O dado '{dado_retrincao}' faz parte de qual coluna? ").lower().strip()
    dado_novo = input("Qual o novo dado você quer inserir? ").title().strip()
    coluna_com_novo_dado = input(f"Qual coluna o '{dado_novo}' vai ser inserido? ").lower().strip()

    if coluna_restricao == "id":
        coluna_restricao = "pk_id"
    elif coluna_restricao == "tipo de exemplar":
        coluna_restricao = "tipo_exemplar"
    elif coluna_restricao == "diretor" or coluna_restricao == "autor":
        coluna_restricao = "criador"
    elif coluna_restricao == 'quantidade':
        try:
            dado_retrincao = int(dado_retrincao)
        except:
            print("Erro ao transforma em inteiro")

    if coluna_com_novo_dado == "tipo de exemplar":
        coluna_com_novo_dado = "tipo_exemplar"
    elif coluna_com_novo_dado == "diretor" or coluna_com_novo_dado == "autor":
        coluna_com_novo_dado = "criador"
    elif coluna_com_novo_dado == 'quantidade':
        try:
            dado_novo = int(dado_novo)
        except:
            print("Erro ao transforma em inteiro")


    comando_sql = f'update tb_amostras set {coluna_com_novo_dado}=? where {coluna_restricao}=?'

    try:
        cur.execute(comando_sql, (dado_novo, dado_retrincao))
        conexao.commit()
    except Error as e:
        print("Erro no comando sql")
        print(e)
        conexao.rollback()
    except:
        print("Erro ao atualizar tabela")
    else:
        print()
        print("Tabela atualizada com sucesso")

    return


def deletar():
    linha_apagar = input("Você quer a apagar a linha que está com qual dado? ").title().strip()
    coluna_com_dado = input(f"O dado {linha_apagar} está em qual coluna? ").lower().strip()

    if coluna_com_dado == "id":
        coluna_com_dado = "pk_id"
    elif coluna_com_dado == "tipo de exemplar":
        coluna_com_dado = "tipo_exemplar"
    elif coluna_com_dado == "diretor" or coluna_com_dado == "autor":
        coluna_com_dado = "criador"
    elif coluna_com_dado == 'quantidade':
        try:
            linha_apagar = int(linha_apagar)
        except:
            print("Erro ao transforma em inteiro")

    comando_sql = f'delete from tb_amostras where {coluna_com_dado}=?'

    try:
        cur.execute(comando_sql, (linha_apagar, ))
        conexao.commit()
    except Error as e:
        print("Erro no comando sql")
        print(e)
    except:
        print("Erro deletar dado")
    else:
        print()
        print("Dado deletado com sucesso")

    return


if __name__ == '__main__':
    conexao = sqlite3.connect('Coleção')
    cur = conexao.cursor()

    try:
        cur.execute("create table if not exists tb_amostras("
                    "pk_id integer primary key autoincrement,"
                    "nome text,"
                    "tipo_exemplar text,"
                    "criador text,"
                    "quantidade int)")
        conexao.commit()
    except Error as e:
        print("Erro a criar tabela")
        print(e)
        cur.close()
        conexao.close()

    opcao = -1
    while opcao != 0:
        print()

        opcao = -1
        while 0 > opcao or opcao > 4:
            try:
                opcao = int(input("1 - Inserir \n2 - Ver aquivos \n3 - Atualizar \n4 - Deletar \nEscolha sua opção: "))
            except:
                print("Tente novamente, escolha um dos números")
                continue

        print()

        if opcao == 1:
            inserir()
        elif opcao == 2:
            selecionar()
        elif opcao == 3:
            atualizar()
        elif opcao == 4:
            deletar()