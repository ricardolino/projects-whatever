def soma(n):
    if n == 1:
        return n
    return soma(n-1) + n

print(soma(5))
