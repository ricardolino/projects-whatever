from random import *
from time import *

lista = [1, 2, 3]
print("O computador irá sortear um número de 1 a 3 tente a divinhar...")
sleep(5)
print("Sorteando...")
premio = choice(lista)
sleep(3)
escolha = int(input("Escolha o seu número (de 1 a 3): "))
print(f"Você escolheu o número {escolha}.")

if escolha == premio:
    lista.remove(escolha)
else:
    lista.remove(premio)
    lista.remove(escolha)

print()
dica = choice(lista)
sleep(3)
print(f"\nVou te dar uma DICA. O número {dica} NÃO é o sorteado.")
sleep(3)
troca = int(input(f"Você deseja trocar sua opção ?"
                  f"\n[1]Sim e [2]Não: "))

if troca == 1:
    if escolha == premio:
        lista.remove(dica)
        escolha = lista[0]
    else:
        escolha = premio

print()
if escolha == premio:
    print("Parabens vc acertou")
else:
    print(f"Infelizmente vc ERROU, o número sorteado era {premio}")
