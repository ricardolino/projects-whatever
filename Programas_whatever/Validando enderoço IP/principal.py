def ler_arquivo():

    try:
        arquivo = open('arquivo de entrada', 'r')
    except:
        print("Erro ao ler arquivo de entrada")
        arquivo.close()
        return

    lista = arquivo.readlines()

    arquivo.close()

    return lista

def validarIP(lista):

    validos = []
    invalidos = []

    for i in lista:
        validação = True
        ip = i.split('.')

        for j in ip:
            if 0 > int(j) or int(j) > 255:
                validação = False
                break

        ip = '.'.join(ip)

        if validação:
            validos.append(ip)
        else:
            invalidos.append(ip)

    return validos, invalidos

def escrever_arquivo(lista_validos, lista_invalidos):

    try:
        arquivo = open('arquivo de saida.txt', 'w')
    except:
        print("Erro ao criar arquivo de saída")
        arquivo.close()
        return

    arquivo.write("[Endereços válidos: ]\n")
    arquivo.writelines(lista_validos)

    arquivo.write("\n[Endereços invalidos: ]\n")
    arquivo.writelines(lista_invalidos)

    arquivo.close()

    return


if __name__ == '__main__':
    listaIP = ler_arquivo()

    listaIP_validos, listaIP_invalidos = validarIP(listaIP)

    escrever_arquivo(listaIP_validos, listaIP_invalidos)